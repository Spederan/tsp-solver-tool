
//# General Utilities
const floor = (number, N = 0) => {
    const multiplier = Math.pow(10, Math.floor(N));
    return Math.floor(number * multiplier) / multiplier;
};

const ceil = (number, N = 0) => {
    const multiplier = Math.pow(10, Math.floor(N));
    return Math.ceil(number * multiplier) / multiplier;
};

const round = (number, N = 0) => {
    const multiplier = Math.pow(10, Math.floor(N));
    return Math.round(number * multiplier) / multiplier;
};

const isUndefined = (val) => {
    return val === undefined;
};

const random = (min, max) => Math.random()*(max - min)+min;//! Nondeterministic random function

const isClient = () => {
    try {
        return typeof window !== 'undefined';
    } catch (e) {}
    return false;
};

const degreesToRadians = (degrees) => degrees * (Math.PI / 180);

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

function arraysSortedEqual(a2, b2) {
    let a = [...a2];
    let b = [...b2];
    a.sort((x,y)=>x-y);
    b.sort((x,y)=>x-y);
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
};

/*
// A function to calculate how horizontal, vertical, and diagonal something is
function calculateAxiality(rad) {
    const max = Math.PI;
    let radians = convertToPositiveRadians(rad) % max;
    let horizontality;
    let verticality;
    let diagonality;
    const rate = radians/max;
    if (rate <= 0.25){
        diagonality = rate * 4;
        verticality = 1 - rate*2;
        horizontality = 1 - verticality;
    };
    if (rate > 0.25 && rate <= 0.5){
        diagonality = (1 - (rate * 2))*2;
        verticality = 1 - rate*2;
        horizontality = 1 - verticality;
    };
    if (rate > 0.5 && rate <= 0.75){
        diagonality = (rate - 0.5) * 4;
        horizontality = 1 - (rate-0.5)*2;
        verticality = 1 - horizontality;
    };
    if (rate > 0.75){
        diagonality = (1 - ((rate - 0.5) * 2))*2;
        horizontality = 1 - (rate-0.5)*2;
        verticality = 1 - horizontality;
    };
    let indiagonality = 1 - diagonality;
    let semidiagonality = (diagonality * indiagonality) * 4;
    let semiindiagonality = 1 - semidiagonality
    return [horizontality, verticality, diagonality, indiagonality, semidiagonality, semiindiagonality];
};*/

//# Algorithms

// Rotate points after translating them to the origin, then translate back and offset them by x and y
function _processPoints(obj) {
    const translatedPoints = obj.points.map(({ x, y }) => ({
        x: x - obj.w / 2,
        y: y - obj.h / 2
    }));
    const rotatedPoints = translatedPoints.map(({ x, y }) => ({
        x: x * Math.cos(obj.radians) - y * Math.sin(obj.radians),
        y: x * Math.sin(obj.radians) + y * Math.cos(obj.radians)
    }));
    const finalPoints = rotatedPoints.map(({ x, y }) => ({
        x: x + obj.w / 2 + obj.x,
        y: y + obj.h / 2 + obj.y
    }));
    return finalPoints;
};

// Function to check if two convex polygons are colliding using Separating Axis Theorem
function _getEdges(points) {
    const edges = [];
    for (let i = 0; i < points.length; i++) {
        const p1 = points[i];
        const p2 = points[(i + 1) % points.length];
        edges.push({ x: p2.x - p1.x, y: p2.y - p1.y });
    }
    return edges;
};

function _projectPointsOntoAxis(points, axis) {
    let min = Number.MAX_VALUE;
    let max = Number.MIN_VALUE;
    const lengthSquared = axis.x * axis.x + axis.y * axis.y;
    const normalAxis = { x: axis.x / lengthSquared, y: axis.y / lengthSquared };
    for (const point of points) {
        const dotProduct = point.x * normalAxis.x + point.y * normalAxis.y;
        min = Math.min(min, dotProduct);
        max = Math.max(max, dotProduct);
    };
    return { min, max };
};

function _isOverlap(projectionA, projectionB, epsilon = 1e-10) {
    return projectionA.max + epsilon >= projectionB.min && projectionA.min - epsilon <= projectionB.max;
};

function separatingAxisTheorem(p1, p2){
    const edges = _getEdges(p1).concat(_getEdges(p2));
    for (const edge of edges) {
        const axis = { x: -edge.y, y: edge.x };
        const projection1 = _projectPointsOntoAxis(p1, axis);
        const projection2 = _projectPointsOntoAxis(p2, axis);
        if (!_isOverlap(projection1, projection2)) {
            return false;
        };
    };
    return true;
};

//? Custom Collision Detection Algorithm

function calculateSidesFromPoints(points) {
    const sides = [];
    const set = new Set();
    const lineSet = [];
    let lineSetCounter = 0;
    for (let i = 0; i < points.length; i++) {
        const point = `${points[i].x},${points[i].y}`;
        if (!set.has(point)){
            set.add(point);
            if (!lineSet[lineSetCounter]) lineSet[lineSetCounter] = [];
            lineSet[lineSetCounter].push(points[i]);
        } else {
            lineSetCounter++;
        };
    };
    for (let s = 0; s < lineSet.length; s++){
        const points = lineSet[s];
        for (let i = 0; i < points.length; i++) {
            const point1 = points[i];
            const point2 = i === points.length - 1 ? points[0] : points[i + 1];
            const side = {
                x1: point1.x,
                y1: point1.y,
                x2: point2.x,
                y2: point2.y,
            };
            sides.push(side);
        };
    };
    return sides;
};

function doLinesIntersect(A1, A2, B1, B2) {
    const epsilon = 2e-10;
    const val1 = (A2.y - A1.y) * (B1.x - A2.x) - (A2.x - A1.x) * (B1.y - A2.y);
    const val2 = (A2.y - A1.y) * (B2.x - A2.x) - (A2.x - A1.x) * (B2.y - A2.y);
    const val3 = (B2.y - B1.y) * (A1.x - B2.x) - (B2.x - B1.x) * (A1.y - B2.y);
    const val4 = (B2.y - B1.y) * (A2.x - B2.x) - (B2.x - B1.x) * (A2.y - B2.y);
    if (((val1 >= -epsilon && val2 <= epsilon) || (val1 <= epsilon && val2 >= -epsilon)) && 
        ((val3 >= -epsilon && val4 <= epsilon) || (val3 <= epsilon && val4 >= -epsilon))){
        return true;
    };
    return false;
};

function doLineSegmentsIntersect(A, B) {
    const val1 = (A.y2 - A.y1) * (B.x1 - A.x2) - (A.x2 - A.x1) * (B.y1 - A.y2);
    const val2 = (A.y2 - A.y1) * (B.x2 - A.x2) - (A.x2 - A.x1) * (B.y2 - A.y2);
    const val3 = (B.y2 - B.y1) * (A.x1 - B.x2) - (B.x2 - B.x1) * (A.y1 - B.y2);
    const val4 = (B.y2 - B.y1) * (A.x2 - B.x2) - (B.x2 - B.x1) * (A.y2 - B.y2);
    if (((val1 >= 0 && val2 <= 0) || (val1 <= 0 && val2 >= 0)) && 
        ((val3 >= 0 && val4 <= 0) || (val3 <= 0 && val4 >= 0))){
        return true;
    };
    return false;
};

function calculateLinesWithinBox(lines, boundingBox) {
    const { x, y, w, h } = boundingBox;
    return lines.filter((line) => {
        const isStartWithinBox = 
            (line.x1 >= x && line.x1 <= x + w && 
            line.y1 >= y && line.y1 <= y + h);
        const isEndWithinBox = 
            (line.x2 >= x && line.x2 <= x + w && 
            line.y2 >= y && line.y2 <= y + h);
        return isStartWithinBox || isEndWithinBox || 
        (doLineSegmentsIntersect(
            { x1: x, y1: y, x2: x + w, y2: y },
            { x1: line.x1, y1: line.y1, x2: line.x2, y2: line.y2 }
        ) || doLineSegmentsIntersect(
            { x1: x + w, y1: y , x2: x + w, y2: y + h },
            { x1: line.x1, y1: line.y1,  x2: line.x2, y2: line.y2 }
        ) || doLineSegmentsIntersect(
            { x1: x + w, y1: y + h,  x2: x, y2: y + h },
            { x1: line.x1, y1: line.y1, x2: line.x2, y2: line.y2 }
        ) || doLineSegmentsIntersect(
            { x1: x, y1: y + h, x2: x, y2: y },
            { x1: line.x1, y1: line.y1, x2: line.x2, y2: line.y2 }
        ));

    });
};

function calculateAreaFromPoints(points) {
    const numPoints = points.length;
    let area = 0;
    for (let i = 0; i < numPoints - 1; i++) {
        area += (points[i].x * points[i + 1].y) - (points[i + 1].x * points[i].y);
    }
    area += (points[numPoints - 1].x * points[0].y) - (points[0].x * points[numPoints - 1].y);
    area = Math.abs(area) / 2;
    return area;
};

function calculateCircularBoundingBox(obj) {
    const points = _processPoints(obj);
    const centroid = points.reduce((acc, point) => ({ x: acc.x + point.x, y: acc.y + point.y }), { x: 0, y: 0 });
    centroid.x /= points.length;
    centroid.y /= points.length;
    const radius = points.reduce((maxDistance, point) => {
        const distance = Math.sqrt((point.x - centroid.x) ** 2 + (point.y - centroid.y) ** 2);
        return Math.max(maxDistance, distance);
    }, 0);
    return {
        x: centroid.x - radius,
        y: centroid.y - radius,
        w: radius*2,
        h: radius*2,
        r: radius,
    };
};

function circularIntersectionArea(X1, Y1, R1, X2, Y2, R2){
    let Pi = Math.PI;
    let d, alpha, beta, a1, a2;
    let ans;
    d = Math.sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1));
    if (d > R1 + R2)
        ans = 0;
    else if (d <= (R1 - R2) && R1 >= R2)
        ans = Math.floor(Pi * R2 * R2);
    else if (d <= (R2 - R1) && R2 >= R1)
        ans = Math.floor(Pi * R1 * R1);
    else {
        alpha = Math.acos((R1 * R1 + d * d - R2 * R2)
                     / (2 * R1 * d))
                * 2;
        beta = Math.acos((R2 * R2 + d * d - R1 * R1)
                    / (2 * R2 * d))
               * 2;
        a1 = 0.5 * beta * R2 * R2
             - 0.5 * R2 * R2 * Math.sin(beta);
        a2 = 0.5 * alpha * R1 * R1
             - 0.5 * R1 * R1 * Math.sin(alpha);
        ans = Math.floor(a1 + a2);
    };
    return ans;
};

function isPointsRectangle(points, boundingBox) {
    const { x: bx, y: by, w: bw, h: bh } = boundingBox;
    const cornerPoints = [
        { x: bx, y: by },
        { x: bx + bw, y: by },
        { x: bx + bw, y: by + bh },
        { x: bx, y: by + bh }
    ];
    for (const point of points) {
        if (!cornerPoints.some(corner => corner.x === point.x && corner.y === point.y)) {
            return false;
        };
    };
    return true;
}


function calculateIntersectionalBoundingBox(a, b) {
    const [x1, y1, w1, h1] = [a.x, a.y, a.w, a.h];
    const [x2, y2, w2, h2] = [b.x, b.y, b.w, b.h];
    const x = Math.max(x1, x2);
    const y = Math.max(y1, y2);
    const w = Math.max(0, Math.min(x1 + w1, x2 + w2) - x);
    const h = Math.max(0, Math.min(y1 + h1, y2 + h2) - y);
    return { x, y, w, h };
};

function isPointInPolygon(point, obj) {
    const points = _processPoints(obj)
    let intersections = 0;
    for (let i = 0, j = points.length - 1; i < points.length; j = i++) {
        const vi = points[i];
        const vj = points[j];
        if ((vi.y === point.y && vj.y === point.y) &&
            ((vi.x <= point.x && point.x <= vj.x) || (vj.x <= point.x && point.x <= vi.x))) {
            return true;
        };
        if (
            ((vi.y < point.y && vj.y >= point.y) || (vj.y < point.y && vi.y >= point.y)) &&
            (vi.x + (point.y - vi.y) / (vj.y - vi.y) * (vj.x - vi.x) < point.x)
        ) {
            intersections++;
        };
    };
    return intersections % 2 === 1;
};

function intersectingSidesTheorem(a, b){
    /* _________________________________________________________________________________________________

    A collision detection algorithm for arbitrarily shaped polygons, including concave ones.
    It has a worst case time complexity of O(n^2) (n being number of sides or vertices) but because
    we filter the points and sides and break early, its likely to be far more efficient, and linear on average. 
    ___________________________________________________________________________________________________*/
    
    /* Define points, area, sides, and other values, but skip unneeded calculations */
    const ab = [a,b];
    for (let obj of ab){
        if (!obj._IST || obj.w !== obj._IST.prevWidth || obj.h !== obj._IST.prevHeight){
            obj._IST = {};
            obj._IST.points = _processPoints(obj);
            obj._IST.sides = calculateSidesFromPoints(obj._IST.points);
            obj._IST.boundingBox = calculateBoundingBox(obj);
            obj._IST.performCircularCheck = (Math.min(obj.w, obj.h)/Math.max(obj.w,obj.h)) >= 2/3;
            if (obj._IST.performCircularCheck) obj._IST.circularBoundingBox = calculateCircularBoundingBox(obj);
            obj._IST.isCircle = obj.sides >= Infinity && obj.w === obj.h;
            if (obj.points.length === 4){
                const bb = calculateBoundingBox({x: obj.x, y: obj.y, w: obj.w, h: obj.h, radians: 0, points: obj.points});
                obj._IST.isRectangle = isPointsRectangle(obj.points, {x: bb.x - obj.x, y: bb.y - obj.y, w: bb.w, h: bb.h})
            } else {
                obj._IST.isRectangle = false;
            };
            obj._IST.prevRadians = obj.radians;
            obj._IST.prevWidth = obj.w;
            obj._IST.prevHeight = obj.h;
            obj._IST.prevX = obj.x;
            obj._IST.prevY = obj.y;
            obj._IST.area = calculateAreaFromPoints(obj._IST.points);
            obj._IST.unchanged = false;
            obj._IST.collisionMemory = new Set();
        };
        if (obj.radians !== obj._IST.prevRadians || obj._IST.prevX !== obj.x || obj._IST.prevY !== obj.y){
            obj._IST.prevX = obj.x;
            obj._IST.prevY = obj.y;
            obj._IST.points = _processPoints(obj);
            obj._IST.sides = calculateSidesFromPoints(obj._IST.points);
            obj._IST.boundingBox = calculateBoundingBox(obj);
            obj._IST.circularBoundingBox = calculateCircularBoundingBox(obj);
            obj._IST.unchanged = false;
            obj._IST.collisionMemory.clear();
        } else {
            obj._IST.unchanged = true;
        };
    };

    //? Define Bounding Boxes

    const boundingBoxA = a._IST.boundingBox;
    const boundingBoxB = b._IST.boundingBox; 

    //? Memory-Check Filter
    /* 
        Explanation: We check to see if two objects have each other in memory, and if so, we can skip further checking.

        Runs in O(1) constant time by itself, but it will only reduce the entire algorithm to O(1) time 
            if the objects' size, position, and orientation does not change.
    */
   if (a._IST.unchanged && b._IST.unchanged && a._IST.collisionMemory.has(b)) return true;

   //? Circle Handling
    /* 
        Explanation: If two objects are determined to be circles, we perform a circle check, which is much faster
            than anything else we might do below. Determination of circularity is based on width === height and the 
            object being tagged as having "infinite sides".

        Additionally we can check circles and rectangles, including rotated ones.

        Runs in O(1) constant time, in regards to the number of sides/points.
    */
   if (a._IST.isCircle && b._IST.isCircle){
        const [circle1, circle2] = [a,b];
        const dx = circle2.x - circle1.x;
        const dy = circle2.y - circle1.y;
        const distance = Math.sqrt(dx * dx + dy * dy);
        return distance <= circle1.w/2 + circle2.w/2;
   };
   let aCircleBRectangle = a._IST.isCircle && b._IST.isRectangle;
   let aRectangleBCircle = b._IST.isCircle && a._IST.isRectangle;
   if (aRectangleBCircle){
        [a,b] = [b,a];
        aCircleBRectangle = true;
   };
   if (aCircleBRectangle){
        const [circle, rectangle] = [a,b];
        if (rectangle.radians % Math.PI === 0) {
            const closestX = Math.max(rectangle.x - rectangle.w / 2, Math.min(circle.x, rectangle.x + rectangle.w / 2));
            const closestY = Math.max(rectangle.y - rectangle.h / 2, Math.min(circle.y, rectangle.y + rectangle.h / 2));
            const distanceX = circle.x - closestX;
            const distanceY = circle.y - closestY;
            return Math.sqrt(distanceX * distanceX + distanceY * distanceY) <= circle.w/2;
        } else {
            const dx = circle.x - rectangle.x;
            const dy = circle.y - rectangle.y;
            const cosTheta = Math.cos(-rectangle.radians);
            const sinTheta = Math.sin(-rectangle.radians);
            const rotatedX = cosTheta * dx - sinTheta * dy;
            const rotatedY = sinTheta * dx + cosTheta * dy;
            const closestX = Math.max(-rectangle.w / 2, Math.min(rotatedX, rectangle.w / 2));
            const closestY = Math.max(-rectangle.h / 2, Math.min(rotatedY, rectangle.h / 2));
            const unrotatedX = cosTheta * closestX + sinTheta * closestY + rectangle.x;
            const unrotatedY = -sinTheta * closestX + cosTheta * closestY + rectangle.y;
            const distanceX = circle.x - unrotatedX;
            const distanceY = circle.y - unrotatedY;
            return Math.sqrt(distanceX * distanceX + distanceY * distanceY) <= circle.w/2;
        };      
   };

    //? Area-Check Filter 
    /* 
        Explanation: We precalculate the area of each object, and then check to see if the area of the bounding box 
            intersection surpasses the greater negative area of the two objects. If so, it by definition must have a 
            collision, and no further collision checks are necessary. We use both a rectangle, and a circle.

        Rectangles are better for rectangles and nonrotated elongated shapes, but circles are better for many-sided 
            regular polygons and other circles.

        Runs in O(1) constant time by itself, but the overall algorithm's time is still O(n).
    */
    {
        const intersectionalBoundingBox = calculateIntersectionalBoundingBox(boundingBoxA, boundingBoxB);
        const intersectionalArea = intersectionalBoundingBox.w * intersectionalBoundingBox.h;
        const [areaA, areaB] = [boundingBoxA.w*boundingBoxA.h, boundingBoxB.w*boundingBoxB.h]
        const negativeSpaceA = areaA - a._IST.area;
        const negativeSpaceB = areaB - b._IST.area;
        if (intersectionalArea > Math.max(negativeSpaceA, negativeSpaceB)) return true;
    };
    if (a._IST.performCircularCheck && b._IST.performCircularCheck && (a.points.length > 4 || b.points.length > 4)) {
        const [circleA, circleB] = [a._IST.circularBoundingBox, b._IST.circularBoundingBox]
        const intersectionalArea = circularIntersectionArea(circleA.x, circleA.y, circleA.r, circleB.x, circleB.y, circleB.r);
        const areaA = Math.PI * circleA.r ** 2;
        const areaB = Math.PI * circleB.r ** 2;
        const negativeSpaceA = areaA - a._IST.area;
        const negativeSpaceB = areaB - b._IST.area;
        if (intersectionalArea > Math.max(negativeSpaceA, negativeSpaceB)) return true;
    };
    

    //? Ray-Casting Vertex
    /* 
        Explanation: We see if a single point from a vertex mirrored beyond the object has an *odd number* of intersections 
            with its sides, to prove that point is colliding with the area within the other object. This handles collisions 
            where an object is inside of another object. 

        Which vertex we use does not matter, and we only need to use one, but rather than selecting the first vertex, we 
            calculate which one is closest to the object, in order to increase the chances of breaking early and not having 
            to check the sides.

        Runs in O(n) time (where n = sides of both objects).
    */

    if (a.w * a.h < b.w * b.h) [a, b] = [b, a];
    const vertices = b._IST.points;
    let closestVertex = vertices[0];
    for (let i = 1; i < vertices.length; i++) {
        const distance = Math.sqrt((a.x - vertices[i].x) ** 2 + (a.y - vertices[i].y) ** 2);
        const currentDistance = Math.sqrt((a.x - closestVertex.x) ** 2 + (a.y - closestVertex.y) ** 2);
        if (distance < currentDistance) {
            closestVertex = vertices[i];
        };
    };
    if (isPointInPolygon(closestVertex, a)) return true;

    //? Sides-Intersecting-Sides
    /*
        Explanation: We filter each object's sides by the bounding box of the other object, then compare the filtered
            sides to each other. This identifies collisions between sides, which constitute the vast majority of collisions. 

        Runs in O(n) time on average, but can degenerate to O(n^2) time (where n = sides of both objects) if
            two objects consist of many vertices and are overlapping significantly, but not enough to trigger the area check.
            In theory, the area checks should relieve computational strain that would come from this in many scenarios.
    */
    const filteredSides1 = calculateLinesWithinBox(a._IST.sides, boundingBoxB);
    const filteredSides2 = calculateLinesWithinBox(b._IST.sides, boundingBoxA);
    if (filteredSides1.length > 0 && filteredSides2.length > 0){
        let intersecting = false;
        for (let i = 0; i < filteredSides1.length; i++){
            for (let j = 0; j < filteredSides2.length; j++){
                intersecting = doLineSegmentsIntersect(filteredSides1[i], filteredSides2[j]);
                if (intersecting) return true;
            };
        };
    };
    return false;
};


function arePolygonsColliding(a, b) {
    let intersecting = intersectingSidesTheorem(a, b);
    if (intersecting){
        a._IST.collisionMemory.add(b);
        b._IST.collisionMemory.add(a);
    };
    return intersecting;
};

// Function to define the points of a rectangle
function defineRectanglePoints(width, height) {
    return [{x: 0,y: 0}, {x: width, y: 0}, {x: width, y: height}, {x: 0, y: height}];
};
function calculateBoundingBox(obj) {
    let points;
    /*let rotate = true;
    //? Handling Ellipses
    if (!obj.points){
        //? Unrotated and Circular Ellipses
        if (!obj.radians || obj.w === obj.h){
            points = defineRectanglePoints(obj.w, obj.h);
            rotate = false;
            points = points.map(({ x, y }) => ({
                x: x + obj.x,
                y: y + obj.y
            }));
        //? Rotated irregular ellipses (approximated as 32-gons with an offset)
        } else {
            const granularity = 32;
            const offset = ((Math.max(obj.w, obj.h) / Math.min(obj.w, obj.h)) + (32/granularity)*2) * (16/granularity);
            const offsetX = offset;
            const offsetY = offset;
            obj.points = calculateRegularPolygonPoints(granularity, obj.w + offsetX, obj.h + offsetY);
            obj.points = obj.points.map(point => ({
                x: point.x - offsetX / 2,
                y: point.y - offsetY / 2,
            }));
        };
    };*/
    points = _processPoints(obj);
    let minX = points[0].x;
    let minY = points[0].y;
    let maxX = points[0].x;
    let maxY = points[0].y;
    for (let i = 1; i < points.length; i++) {
        const { x, y } = points[i];
        minX = Math.min(minX, x);
        minY = Math.min(minY, y);
        maxX = Math.max(maxX, x);
        maxY = Math.max(maxY, y);
    };
    const width = maxX - minX;
    const height = maxY - minY;
    return { x: minX, y: minY, w: width, h: height };
};

const countingSort = (arr, property, min = 0, max = 10000) => {
    const data = new Array(Math.floor(max-min));
    const newArr = new Array(arr.length);
    let counter = 0;
    let value;
    for (let i = 0; i < arr.length; i++){
        value = Math.floor(arr[i][property] - min);
        if (!data[value]) data[value] = [];
        data[value].push(arr[i]);
    };
    data.forEach((value)=>{
        newArr.splice(counter, value.length, ...value);
        counter += value.length;
    });
    return newArr;
};

const efficientCountingSort = (arr, property, min, max) => {
    //? Calculate Min, Max, and Range
        if (!max && !min){
            max = 0;
            min = Infinity;
            for (let i = 0; i < arr.length; i++){
                max = Math.max(max, arr[i][property]);
                min = Math.min(min, arr[i][property]);
            };
        };
        min = Math.floor(min);
        max = Math.floor(max);
        const range = max-min;
    //? Perform Alternative Counting Sort for small ranges
        if (range < 3300){
            return countingSort(arr, property, min, max);
        } else {
            //? Perform Non-Counting Sort for ranges too large compared to input size
                if (range > arr.length*Math.log(arr.length)){
                    arr.sort((a,b)=>Math.floor(a[property]) - Math.floor(b[property]));
                    return arr;
                };
        };
    //? Add object keys to map
        let newArr = [];
        const values = new Map();
        let v;
        for (let i = 0; i < arr.length; i++){
            v = Math.floor(arr[i][property]);
            if (!values.get(v)) values.set(v, []);
            values.get(v).push(arr[i]);
        };
    //? Iterate through range array and add keys to final array
        for (let i = min; i <= max; i++){
            if (values.has(i)){
                v = values.get(i);
                for (let j = 0; j < v.length; j++){
                    newArr.push(v[j]);
                };
            };
        };
    return newArr;
};

const continuousCollision = (items, i, neighbor) => {
    const boundingBoxA = {
        x: items[i].__xPrev + Math.min(items[i].__xSpeed, 0),
        y: items[i].__yPrev + Math.min(items[i].__ySpeed, 0),
        w: items[i].__width + Math.abs(items[i].__xSpeed),
        h: items[i].__height + Math.abs(items[i].__ySpeed)
    };
    const boundingBoxB = {
        x: items[neighbor].__xPrev + Math.min(items[neighbor].__xSpeed, 0),
        y: items[neighbor].__yPrev + Math.min(items[neighbor].__ySpeed, 0),
        w: items[neighbor].__width+ Math.abs(items[neighbor].__xSpeed),
        h: items[neighbor].__height + Math.abs(items[neighbor].__ySpeed)
    };
    if (boundingBoxA.x <= boundingBoxB.x + boundingBoxB.w &&
        boundingBoxA.x + boundingBoxA.w >= boundingBoxB.x &&
        boundingBoxA.y <= boundingBoxB.y + boundingBoxB.h &&
        boundingBoxA.y + boundingBoxA.h >= boundingBoxB.y){
        const neighborMidpointX = ((items[neighbor].__xPrev + items[neighbor].__x)/2 + items[neighbor].__width/2);
        const neighborMidpointY = ((items[neighbor].__yPrev + items[neighbor].__y)/2 + items[neighbor].__height/2);
        const widthOffset = Math.max(items[i].__width, items[neighbor].__width)/2;
        const heightOffset = Math.max(items[i].__height, items[neighbor].__height)/2;
        if (doLinesIntersect(
                { x: items[i].__x, y: items[i].__y}, 
                { x: items[i].__xPrev + items[i].__width, y: items[i].__yPrev + items[i].__height}, 
                { x: neighborMidpointX - widthOffset, y: neighborMidpointY + heightOffset}, 
                { x: neighborMidpointX + widthOffset, y: neighborMidpointY - heightOffset}) ||
            doLinesIntersect(
                { x: items[i].__x + items[i].__width, y: items[i].__y + items[i].__height}, 
                { x: items[i].__xPrev, y: items[i].__yPrev}, 
                { x: neighborMidpointX - widthOffset, y: neighborMidpointY + heightOffset}, 
                { x: neighborMidpointX + widthOffset, y: neighborMidpointY - heightOffset}) ||
            doLinesIntersect(
                { x: items[i].__x - widthOffset, y: items[i].__y - heightOffset}, 
                { x: items[i].__xPrev + items[i].__width- widthOffset, y: items[i].__yPrev + items[i].__height - heightOffset}, 
                { x: neighborMidpointX - widthOffset, y: neighborMidpointY - heightOffset}, 
                { x: neighborMidpointX + widthOffset, y: neighborMidpointY + heightOffset}) 
                ){
            items[i].__collisions.add(items[neighbor].__id);
            items[neighbor].__collisions.add(items[i].__id);
            const leniencyY = Math.abs(items[i].__ySpeed)/(items[i].__height);
            const leniencyX = Math.abs(items[i].__xSpeed)/(items[i].__width);
            const leniency = leniencyX + leniencyY;
            items[i].__collisionCount+= Math.min(1, ((items[neighbor].__width*items[neighbor].__height) / (items[i].__width*items[i].__height))/leniency);
            items[neighbor].__collisionCount+= Math.min(1, ((items[i].__width*items[i].__height) / (items[neighbor].__width*items[neighbor].__height)));
            return true;
        };
    };
    return false;
};


const sweepAndPrune = (items, maxCollisions, optionalCallback) => {
    let neighbor;
    let xCollided;
    let collided;
    let continuouslyCollided1;
    let continuouslyCollided2;
    for (let i = 0; i < items.length; i++){
        neighbor = i + 1;
        xCollided = true;
        //? Check sorted x axis for x collisions but break early when no more are found.
        //# Short-circuiting techniques are used to minimize operations
        while (xCollided){
            xCollided =  
                items[neighbor] &&
                items[i].__highestX + items[i].__width + 1 >= items[neighbor].__lowestX &&
                items[i].__lowestX - 1 <= items[neighbor].__highestX + items[neighbor].__width &&
                items[i].__collisionCount < maxCollisions;
            collided = 
                xCollided && 
                items[i].__y <= items[neighbor].__y + items[neighbor].__height &&
                items[i].__y + items[i].__height >= items[neighbor].__y &&
                items[i].__x + items[i].__width >= items[neighbor].__x &&
                items[i].__x <= items[neighbor].__x + items[neighbor].__width &&
                !items[i].__collisions.has(items[neighbor].__id) && 
                ((!items[i].__checkSAT && !items[neighbor].__checkSAT) || arePolygonsColliding(items[i], items[neighbor]));
                //! Change
            if (collided){
                if (optionalCallback) optionalCallback(items[i], items[neighbor]);
                items[i].__collisions.add(items[neighbor].__id);
                items[neighbor].__collisions.add(items[i].__id);
                items[i].__collisionCount+= Math.min(1, ((items[neighbor].__width*items[neighbor].__height) / (items[i].__width*items[i].__height)));
                items[neighbor].__collisionCount+= Math.min(1, ((items[i].__width*items[i].__height) / (items[neighbor].__width*items[neighbor].__height)));
            } else if (xCollided && (items[i].__continuous || items[neighbor].__continuous)) {
                continuouslyCollided1 = false;
                continuouslyCollided2 = false;
                if (items[i].__continuous)
                    continuouslyCollided1 = continuousCollision(items, i, neighbor);
                if (items[neighbor].__continuous) 
                    continuouslyCollided2 = continuousCollision(items, neighbor, i);
                xCollided = xCollided || continuouslyCollided1 || continuouslyCollided2;
            };                        
            neighbor++;
        };
    };
};

let init = true;
let testBoxes = [];

//? A near-linear and efficient continuous collision detection algorithm.
const detectCollisions = (ogItems, callback, maxCollisions = 8, sequenceDiscrete = false) => {
    // IMPORTANT NOTES: 
    // -------------------------------------------------------------------------------------------------------------
    // .x, .y, .width/.w, and .height/.h are required properties. You can spell out width and height, or use w and h.
    // -------------------------------------------------------------------------------------------------------------
    // .points is a reserved keyword for a list of relative points, necessary for polygonal collision detection to work.
    // -------------------------------------------------------------------------------------------------------------
    // Any property with a double underscore are important to the algorithm, and will be added to the items.
    // Do not manually modify variables with double underscores, unless you want the algorithm to lose functionality.
    // -------------------------------------------------------------------------------------------------------------
    // maxCollisions is a number constant restricting excessive collisions. 8 is the minimum for non-overlapping objects.
    // -------------------------------------------------------------------------------------------------------------
    // sequenceDiscrete is a boolean constant to determine whether or not discrete collisions need to happen in 
    //    order of distance. Its false by default as it involves a large loss in performance and most usecases dont need it.
    // -------------------------------------------------------------------------------------------------------------
    // You can also use this algo to detect collisions between rotated rectangles, they just need a .radians property.
    // -------------------------------------------------------------------------------------------------------------
    let items = [...ogItems];
    let xMin = Infinity;
    let xMax  = 0;
    let yMax = 0;
    let yMin = Infinity;
    let hMin = Infinity;
    let wMin = Infinity;
    let heights = 0;
    let avgHeight;
    let minPartition;
    let maxPartition;
    for (let i = 0; i < items.length; i++){
        items[i].__width = items[i].width || items[i].w;
        items[i].__height = items[i].height || items[i].h;
        if (items[i].radians || items[i].sides !== 4){
            const boundingBox = calculateBoundingBox(items[i]);
            items[i].__checkSAT = true;
            items[i].__x = boundingBox.x;
            items[i].__y = boundingBox.y;
            items[i].__width = boundingBox.w;
            items[i].__height = boundingBox.h;
        } else {
            items[i].__checkSAT = false;
            items[i].__x = items[i].x;
            items[i].__y = items[i].y;
        };
        if (items[i].__xPrev == undefined || items[i].__yPrev == undefined){
            items[i].__xPrev = items[i].__x;
            items[i].__yPrev = items[i].__y;
            items[i].__rPrev = items[i].radians || 0;
            items[i].__initial = true;
        };
        items[i].__xSpeed = items[i].__x - items[i].__xPrev;
        items[i].__ySpeed = items[i].__y - items[i].__yPrev;
        items[i].__rSpeed = items[i].radians - items[i].__rPrev;
        items[i].__lowestX = Math.min(items[i].__x, items[i].__xPrev);
        items[i].__highestX = Math.max(items[i].__x, items[i].__xPrev);
        yMax = Math.max(items[i].__y, yMax);
        yMin = Math.min(items[i].__y, yMin);
        xMax = Math.max(items[i].__highestX, xMax);
        xMin = Math.min(items[i].__lowestX, xMin);
        wMin = Math.min(items[i].__width, wMin);
        hMin = Math.min(items[i].__height, hMin);
        heights+= items[i].__height;
        items[i].__collisions = new Set();
        items[i].__collisionCount = 0;
    };
    items = efficientCountingSort(items, "__lowestX", xMin, xMax);
    for (let i = 0; i < items.length; i++){
        items[i].__id = i;
        items[i].__continuous = (Math.abs(items[i].__ySpeed) >= hMin || Math.abs(items[i].__xSpeed) >= wMin);
    };
    //? Define Y-Axis Partitions. 
        avgHeight = Math.ceil(heights/items.length);
        const squareRootOfN = Math.floor(Math.sqrt(items.length));
        const fitToObjectHeights = Math.ceil(((yMax - yMin) / avgHeight)/2);
        const numberOfPartitions = Math.min(squareRootOfN, fitToObjectHeights);
        const partitions = new Array(numberOfPartitions).fill().map(() => []);
        let partitionSize = 0;
        let minYVal = 0;
        for (let i = 0; i < items.length; i++){
            partitionSize = (yMax - yMin) / numberOfPartitions;
            minYVal = Math.min(items[i].__y, items[i].__yPrev);
            maxYVal = Math.max(items[i].__y, items[i].__yPrev);
            minPartition = Math.floor((minYVal - yMin) / partitionSize);
            maxPartition = Math.floor((maxYVal + items[i].__height - yMin) / partitionSize);
            // Edge Case Limits
                minPartition = Math.max(minPartition, 0); //Lower limit for min
                minPartition = Math.min(minPartition, numberOfPartitions-1); //Upper limit for min
                maxPartition = Math.min(maxPartition, numberOfPartitions-1); //Upper limit for max
                maxPartition = Math.max(maxPartition, minPartition); //Max cant be lower than min
            for (let p = minPartition; p <= maxPartition; p++){
                partitions[p].push(items[i]);
            };
        };
    //? Define optional immediate callback
        let immediateCallback;
        if (!sequenceDiscrete){
            immediateCallback = callback;
        } else {
            immediateCallback = false;
        };
    //? Manage Y-Axis Partitions.
        for (let p = 0; p < partitions.length; p++){
            sweepAndPrune(partitions[p], maxCollisions, immediateCallback);
        };
        for (let i = 0; i < items.length; i++){
            //? Define previous x and y positions for next frame. Necessary for continuous collision detection.
                items[i].__xPrev = items[i].__x;
                items[i].__yPrev = items[i].__y;
                items[i].__rPrev = items[i].radians;
                items[i].__initial = false;
            //? Execute collisions in order of distance from object, to preserve the physical order of events
            //? Note, this does slow down the algorithm in some situations, up to 50%. But the accuracy is much greater.
            if (sequenceDiscrete || items[i].__continuous){
                let collisions = [...items[i].__collisions];
                if (collisions.length > 1){
                    for (let j = 0; j < collisions.length; j++){
                        const obj1 = items[i];
                        const obj2 = items[collisions[j]];
                        obj2.__tempDist = 
                            Math.sqrt((obj2.__x - obj1.__x)**2 + (obj2.__y - obj1.__y)**2 + 
                            (obj2.__width - obj1.__width)**2 + (obj2.__height - obj1.__height)**2);
                    };
                    collisions.sort((a,b) => a.__tempDist - b.__tempDist);
                    for (let j = 0; j < collisions.length; j++){
                        callback(items[i], items[collisions[j]]);
                        items[i].__collisions.delete(collisions[j]);
                        items[collisions[j]].__collisions.delete(items[i].__id);
                    };
                } else if (collisions.length === 1) {
                    callback(items[i], items[collisions[0]]);
                    items[i].__collisions.delete(collisions[0]);
                    items[collisions[0]].__collisions.delete(items[i].__id);
                };
            };
        };
};

function calculateRegularPolygonPoints(sides, width, height) {
    const points = [];
    const offsetX = width / 2;
    const offsetY = height / 2;
    for (let i = 0; i < sides; i++) {
        const angle = (((i / sides) * Math.PI * 2) - Math.PI / 2);  // Adjust the initial angle to avoid rotation
        const x = offsetX + (width / 2) * Math.cos(angle);
        const y = offsetY + (height / 2) * Math.sin(angle) ;
        points.push({ x, y });
    };
    return points;
};

function scalePoints(points, width, height) {
    // Find the bounding box of the points
    let minX = Infinity,
        minY = Infinity,
        maxX = -Infinity,
        maxY = -Infinity;

    for (const point of points) {
        minX = Math.min(minX, point.x);
        minY = Math.min(minY, point.y);
        maxX = Math.max(maxX, point.x);
        maxY = Math.max(maxY, point.y);
    }

    // Calculate scaling factors for width and height
    const scaleX = width / (maxX - minX);
    const scaleY = height / (maxY - minY);

    // Scale and translate each point
    const scaledPoints = points.map(point => ({
        x: (point.x - minX) * scaleX,
        y: (point.y - minY) * scaleY,
    }));

    return scaledPoints;
}

function generateStar(width, height, ratio = 2, numPoints = 5) {
    const centerX = width / 2;
    const centerY = height / 2;
    const outerRadius = (width + height)/4;
    const innerRadius = outerRadius/ratio;
    const points = [];
    for (let i = 0; i < numPoints * 2; i++) {
        const radius = i % 2 === 0 ? outerRadius : innerRadius;
        let theta = ((i / numPoints) * Math.PI);
        if (numPoints === 5) theta-= 0.628/2;

        const pointX = centerX + radius * Math.cos(theta);
        const pointY = centerY + radius * Math.sin(theta);

        points.push({ x: pointX, y: pointY });
    };
    const xOffset = points.reduce((sum, point) => sum + point.x, 0) / points.length;
    const yOffset = points.reduce((sum, point) => sum + point.y, 0) / points.length;
    for (let i = 0; i < points.length; i++) {
        points[i].x -= xOffset - centerX;
        points[i].y -= yOffset - centerY;
    }
    return points;
};

function defineFrame(width, height, holeWidth, holeHeight) {
    const outerRectangle = [
        { x: 0, y: 0 },
        { x: width, y: 0 },
        { x: width, y: height },
        { x: 0, y: height },
    ];
    const innerRectangle = [
        { x: 0 + holeWidth/2, y: 0 + holeHeight/2 },
        { x: width - holeWidth/2, y: 0 + holeHeight/2 },
        { x: width - holeWidth/2, y: height - holeHeight/2 },
        { x: 0 + holeWidth/2, y: height - holeHeight/2 },
    ];
    return [outerRectangle, innerRectangle];
};

function generateRing(width, height, n = 32, m = 32, sizeRatio = 0.75){
    const [innerWidth, innerHeight] = [width*sizeRatio, height*sizeRatio];
    let rectangularFrame;
    if (n === 4 || m === 4) rectangularFrame = defineFrame(width, height, width*sizeRatio, height*sizeRatio);
    let exteriorPoints;
    let interiorPoints;
    if (n === 4){
        exteriorPoints = rectangularFrame[0];
    } else {
        exteriorPoints = calculateRegularPolygonPoints(n, width, height);
    };
    exteriorPoints.push(exteriorPoints[0]);
    if (m === 4){
        interiorPoints = rectangularFrame[1];
        interiorPoints.push(interiorPoints[0]);
    } else {
        interiorPoints = calculateRegularPolygonPoints(m, innerWidth, innerHeight);
        interiorPoints.push(interiorPoints[0]);
        interiorPoints = interiorPoints.map(point => ({
            x: point.x - (innerWidth - width) / 2,
            y: point.y - (innerHeight - height) / 2
        }));
    };
    
    const frame = exteriorPoints.concat(interiorPoints.reverse());
    return frame;
};

function calculateWidth(obj) {
    const points = _processPoints(obj);
    if (!Array.isArray(points) || points.length < 2) {
        return 0; // Invalid input
    };
    let minX = points[0].x;
    let maxX = points[0].x;
    for (let i = 1; i < points.length; i++) {
        const x = points[i].x;
        minX = Math.min(minX, x);
        maxX = Math.max(maxX, x);
    };
    const width = maxX - minX;
    return width;
};

function createLinearRect(x1, y1, x2, y2, width) {
    const length = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
    const angle = Math.atan2(y2 - y1, x2 - x1);

    const centerX = (x1 + x2) / 2;
    const centerY = (y1 + y2) / 2;

    return {
        x: centerX - length / 2,
        y: centerY - width / 2,
        width: length,
        height: width,
        radians: angle,
    };
}




//! Temp
function generateRandomPoints(quantity, width, height) {
    const points = [];
    for (let i = 0; i < quantity; i++) {
        const randomX = Math.random() * width;
        const randomY = Math.random() * height;
        points.push({ x: randomX, y: randomY });
    };
    return points;
};


//# Classes

class Box {
    constructor(htmlMode = false){
        this.htmlMode = htmlMode;
        this.x = 0;
        this.y = 0;
        this.w = 0;
        this.h = 0;
        this.sides = 4;
        this.radians = 0;
        this.callback = null;
        this.clientside = isClient();
        this._type(this.sides);
        this._draw();
        this._redraw();
        _objects.push(this);
        return this;
    };
    _draw(){
        if (this.clientside){
            if (this.htmlMode){
                this.graphics = document.createElement("div");
                this.graphics.id = _objects.length;
                this.graphics.style.position = "absolute";
                document.body.appendChild(this.graphics);
            } else {
                _createGraphicsObject((graphics, container)=>{
                    this.graphics = graphics;
                    this.container = container;
                });
            };
        };
    };
    _redraw(){
        if (!this.graphics) return this;
        if (this.htmlMode){        
            //if (this.strokeObj) this._stroke();
        } else {
            if (this.htmlClone){
                this.htmlClone  
                    .position(this.x, this.y)
                    .size(this.w, this.h)
                    .rotate(this.radians, true)
                    .image(this.src, this.rememberedClipping)
            };
            if (this.color){
                this.graphics.clear();
                if (this.strokeObj) this._stroke();
                if (this.color.length === 1){
                    this.graphics.beginFill(rgb(this.color[0].r, this.color[0].g, this.color[0].b));
                };
                this.graphics.alpha = this.color[0].a/255;
                switch(this.sides){
                    case 4: this.graphics.drawRect(0, 0, this.w, this.h); break;
                    default: this.graphics.drawPolygon(this.points); break;
                    case Infinity: this.graphics.drawEllipse(this.w/2, this.h/2, this.w/2, this.h/2); break;
                };            
                this.graphics.endFill();
            };        
            this.position(this.x, this.y);
        };     
        return this;
    };
    text(txt = '', options = {}){
        let changed = false;
        if (!this.textObj) this.textObj = {
            fontSize: 24,
            fontFamily: 'Arial',
            fill: {r: 0, g: 0, b: 0},
            align: 'center',
            wordWrap: true, 
            wordWrapWidth: calculateWidth(this) - (options.fontSize || 24),
            scaling: true,
        };
        for (let i in options){
            if (this.textObj[i] !== options[i]) changed = true;
            this.textObj[i] = options[i];
            if (typeof this.textObj.fill === 'object'){
                changed = true;
                if (this.textObj.fill.r != null){
                    if (this.textObj.fill.g == null) this.textObj.fill.g = this.textObj.fill.r;
                    if (this.textObj.fill.b == null) this.textObj.fill.b = this.textObj.fill.r;
                };
                if (this.textObj.fill[0] != null){
                    if (this.textObj.fill[1] == null) this.textObj.fill[1] = this.textObj.fill[0];
                    if (this.textObj.fill[2] == null) this.textObj.fill[2] = this.textObj.fill[0];
                };
            };
        };
        if (this.clientside){
            if (this.htmlMode){
                this.txt = true;
                const htmlStyle = {
                    'font-size': `${this.textObj.fontSize}px`,
                    'font-family': this.textObj.fontFamily,
                    'color': `rgb(${this.textObj.fill.r}, ${this.textObj.fill.g}, ${this.textObj.fill.b})`,
                    'text-align': this.textObj.align,
                };
                //! Review code with changed settings
                if (this.textObj.align === "center"){
                    htmlStyle['display'] = 'flex';
                    htmlStyle['align-items'] = 'center';
                    htmlStyle['justify-content'] = 'center';
                };
                if (this.newImageElement){
                    for (let style in htmlStyle) {
                        this.newImageElement.style[style] = htmlStyle[style];
                    };
                    this.newImageElement.textContent = txt;
                } else {
                    for (let style in htmlStyle) {
                        this.graphics.style[style] = htmlStyle[style];
                    };
                    this.graphics.textContent = txt;
                };                
                this.textObj.content = txt;
                if (!txt){
                    this.txt = undefined;
                    this.textObj = undefined;
                };
            } else {
                if (this.textObj.content){
                    this.txt.text = txt;
                    for (let i in this.textObj){
                        this.txt.style[i] = this.textObj[i] || this.txt.style[i];
                    };
                    this.txt.position.set(this.w / 2, this.h / 2);
                } else {
                    if (!this.txt){
                        this.txt = new PIXI.Text(txt, this.textObj);
                        this.txt.anchor.set(0.5);
                        this.txt.position.set(this.w / 2, this.h / 2);
                        this.txt.zIndex = 2;
                        this.graphics.addChild(this.txt);
                    };                
                };
                this.textObj.content = txt;
                if (!txt){
                    this.graphics.removeChild(this.txt);
                    this.txt = undefined;
                    this.textObj = undefined;
                };
            };
        };
        return this;
    };
    image(src, clipping = true){
        this.rememberedClipping = clipping;
        if (this.clientside){
            if (this.htmlClone){
                this.src = src;
                return this;
            };
            if (this.htmlMode){
                if (this.gradientString){
                    if (!this.newImageElement) this.newImageElement = document.createElement("div");
                    this.newImageElement.id = _objects.length + '_image';
                    this.newImageElement.style.position = "absolute";
                    this.newImageElement.style.width = this.w+'px';
                    this.newImageElement.style.height = this.h+'px';
                    this.newImageElement.style.backgroundImage = `url(${src})`;
                    this.newImageElement.style.backgroundSize = `${this.w}px ${this.h}px`;
                    if (clipping){
                        const pointsString = this.points.map(point => `${point.x}px ${point.y}px`).join(', ');
                        this.newImageElement.style.clipPath = `polygon(${pointsString})`;
                        this.newImageElement.style.webkitClipPath = `polygon(${pointsString})`;
                    };
                    this.graphics.appendChild(this.newImageElement);
                } else {
                    this.graphics.style.backgroundImage = `url(${src})`;
                    this.graphics.style.backgroundSize = `${this.w}px ${this.h}px`;
                    if (clipping){
                        const pointsString = this.points.map(point => `${point.x}px ${point.y}px`).join(', ');
                        this.graphics.style.clipPath = `polygon(${pointsString})`;
                        this.graphics.style.webkitClipPath = `polygon(${pointsString})`;
                    };
                };
            } else {
                if (this.img && (src !== this.src || !src)){
                    this.graphics.removeChild(this.img);
                };
                if (src && src !== this.src){
                    this.img = PIXI.Sprite.from(src);
                    this.img.anchor.set(0.5);
                    this.img.position.set(this.w/2, this.h/2)
                    this.img.texture.baseTexture.on('loaded', () => {
                        this.imageLoaded = true;
                        this.img.position.set(this.w / 2, this.h / 2);
                        const scaleXToFit = this.w / this.img.width;
                        const scaleYToFit = this.h / this.img.height;
                        this.img.scale.set(scaleXToFit, scaleYToFit);
                        this.img.zIndex = 1;
                        this.graphics.addChild(this.img);
                        if (clipping){
                            if (!this.mask){
                                this.mask = new PIXI.Graphics();
                                this.mask.beginFill(0x000000);
                                this.mask.drawPolygon(this.points);
                                this.graphics.addChild(this.mask);
                            };
                            this.img.mask = this.mask;
                        };
                    });
                    
                };
            };            
            this.src = src;
        };        
        return this;
    };
    gradient(color, radial = false){
        this.color = color;
        this.radial = radial;
        for (let i = 0; i < this.color.length; i++){
            if (this.color[i].r == null) this.color[i].r = 0;
            if (this.color[i].g == null) this.color[i].g = this.color[i].r;
            if (this.color[i].b == null) this.color[i].b = this.color[i].r;
            if (this.color[i].a == null) this.color[i].a = 255;
        };

        if (this.clientside){
            if (this.htmlMode){
                if (this.color.length !== 1) {
                    //? Gradient Colors
                    this.gradientString = this.color.map(color => {
                        return `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a / 255})`;
                    });
                    if (this.radial) {
                        this.gradientString = this.gradientString.reverse();
                    };            
                    this.gradientString = this.gradientString.join(', ');
                    const gradientType = this.radial ? 'radial-gradient' : 'linear-gradient';
                    const gradientArguments = this.radial ? '(circle, ' : '(to right, ';
                    this.graphics.style.backgroundColor = '';
                    this.graphics.style.background = `${gradientType}${gradientArguments}${this.gradientString})`;
                    if (this.src) this.graphics.style.background = `${this.gradientString}, url(${this.src})`;
                }; 
            } else {      
                  //? We create an HTML Element using our Box class to emulate gradience
                  // Note: Pixijs does not natively support gradient colors.
                  const htmlClone = new Box(true);
                  htmlClone.position(this.x, this.y)
                  htmlClone.size(this.w, this.h)
                  if (this.sides !== 4) htmlClone.shape(this.rememberedVal, this.rememberedArgs)
                  htmlClone.gradient(color, radial)
                  if (this.src) htmlClone.image(this.src, this.rememberedClipping);
                  this.htmlClone = htmlClone;
            };
        };
        this._redraw();
        return this;
    };
    
    _type(val = Infinity, ellipseSides = 32){
        if (this.sides === null){
            this.points = scalePoints(this.points, this.w, this.h);
            return this._redraw();
        };
        this.sides = 0;
        val = Math.floor(val);
        if (val === 4){
            this.sides = 4;
            this.points = defineRectanglePoints(this.w, this.h);
        };
        if (val === 3 || (val > 4 && val < Infinity)){
            this.sides = val;
            this.points = calculateRegularPolygonPoints(this.sides, this.w, this.h);
        };
        if (val >= Infinity){
            this.sides = Infinity;
            this.points = calculateRegularPolygonPoints(ellipseSides, this.w, this.h);
        };
        if (this.sides === 0) throw new Error("Invalid Input: Value must be greater than or equal to 3")
        if (this.clientside) this._redraw();
        if (this.htmlMode){
            if (this.points && this.sides !== 4){
                const polygonPoints = this.points.map(point => `${point.x}px ${point.y}px`).join(', ');
                this.graphics.style.clipPath = `polygon(${polygonPoints})`;
            };
        };
        return this;
    };
    shape(val = Infinity, ...args){
        this.rememberedVal = val;
        if (typeof val === "number") return this._type(val);
        switch(val){
            default: throw new Error("Invalid Input: Invalid String.");
            case "ellipse": this._type(Infinity, ...args); break;
            case "triangle": this._type(3); break;
            case "rectangle": this._type(4); break;
            case "pentagon": this._type(5); break;
            case "hexagon": this._type(6); break;
            case "heptagon": this._type(7); break;
            case "octagon": this._type(8); break;
            case "nonagon": this._type(9); break;
            case "decagon": this._type(10); break;
            case "random": 
                this.points = generateRandomPoints(15, this.w, this.h);
                this.sides = null;
                if (this.clientside) this._redraw();
            break;
            case "star": 
                this.points = generateStar(this.w, this.h, ...args);
                this.sides = null;
                if (this.clientside) this._redraw();
            break;
            case "ring": 
                this.points = generateRing(this.w, this.h, ...args);
                this.sides = null;
                if (this.clientside) this._redraw();
            break;
        };
        if (this.htmlMode){
            if (this.points && this.sides !== 4){
                const pointsString = this.points.map(point => `${point.x}px ${point.y}px`).join(', ');
                this.graphics.style.clipPath = `polygon(${pointsString})`;
                this.graphics.style.webkitClipPath = `polygon(${pointsString})`;
            };  
        };
        return this;
    };
    position(x = 0, y = 0){
        this.x = x;
        this.y = y;
        if (this.clientside) {
            if (this.htmlMode){
                this.graphics.style.left = `${this.x}px`;
                this.graphics.style.top = `${this.y}px`;
                if (this.strokeObj) this._stroke();
            } else {
                if (this.radians){
                    this.graphics.pivot.set(this.w/2, this.h/2);
                    this.graphics.rotation = this.radians;
                    this.graphics.position.set(this.x+this.w/2, this.y+this.h/2);
                } else {
                    this.graphics.pivot.set(0,0);
                    this.graphics.position.set(this.x, this.y);
                };
            };
        };
        return this;
    };
    move(x = 0, y = 0){
        return this.position(this.x + x, this.y + y);
    };
    rotate(num, radianMode = false){
        if (!radianMode) num = degreesToRadians(num);
        if (num !== this.radians){
            this.radians = num;
            if (this.clientside){
                if (this.htmlMode){
                    if (this.radians !== 0) {
                        const rotationValue = `rotate(${this.radians}rad)`;
                        this.graphics.style.transform = rotationValue;
                    } else {
                        this.graphics.style.transform = 'none';
                    };
                    if (this.strokeObj) this._stroke();
                } else {
                    this.position(this.x, this.y);
                }
                if (this.txt && this.textObj?.wordWrap){
                    this.text(this.textObj.content);
                };
            };
        };
        return this;      
    };
    spin(num, radianMode = false){
        if (!radianMode) num = degreesToRadians(num);
        return this.rotate(this.radians + num, true);
    };
    size(w = 0, h = 0, min = 2e-10, max = 10000){
        //? Domain of width and height are positive finite numbers
        {
            if (min <= 0) throw Error("Width or height cannot be non-positive.");
            if (max >= Infinity) throw Error("Width or height cannot be infinite.");
            if (w < min) w = min;
            if (w > max) w = max;
            if (h < min) h = min;
            if (h > max) h = max;
        };
        if (this.w !== w || this.h !== h){
            this.w = w;
            this.h = h;
            if (this.prevW == null) this.prevW = this.w;
            if (this.prevH == null) this.prevH = this.h;  
            if (this.points) this.points = scalePoints(this.points, this.w, this.h)
            if (this.clientside){
                if (this.htmlMode){
                    this.graphics.style.width = `${this.w}px`;
                    this.graphics.style.height = `${this.h}px`;
                    this.graphics.style.backgroundSize = `${this.w}px ${this.h}px`;
                    if (this.newImageElement){
                        this.newImageElement.style.width = `${this.w}px`;
                        this.newImageElement.style.height = `${this.h}px`;
                        this.newImageElement.style.backgroundSize = `${this.w}px ${this.h}px`;
                    };
                    if (this.src) this.image(this.src, this.rememberedClipping);
                    if (this.points && this.sides !== 4){
                        const pointsString = this.points.map(point => `${point.x}px ${point.y}px`).join(', ');
                        this.graphics.style.clipPath = `polygon(${pointsString})`;
                        this.graphics.style.webkitClipPath = `polygon(${pointsString})`;
                    };  
                };
                //? Scaling
                let scaledSize = (this.w*this.h)/(this.prevW*this.prevH);
                if (scaledSize > max/10) scaledSize = max/10;
                if (scaledSize < min) scaledSize = min;
                if (this.txt && this.textObj.scaling){
                    this.text(this.textObj.content, {fontSize: this.textObj.fontSize*scaledSize});
                };
                if (this.strokeObj && this.strokeObj.scaling){
                    this.strokeObj.weight = this.strokeObj.weight*scaledSize;
                    this._stroke(this.strokeObj.r, this.strokeObj.g, this.strokeObj.b, this.strokeObj.weight, this.strokeObj.intrinsic);
                };
                if (this.img && this.imageLoaded){
                    if (this.mask){
                        this.mask.width = this.w;
                        this.mask.height = this.h;
                    };
                    this.img.position.set(this.w/2, this.h/2);
                    this.img.width = this.w;
                    this.img.height = this.h;
                };      
                this.prevW = this.w;
                this.prevH = this.h;       
                this._redraw();
            };
        };        
        return this;
    };
    grow(w = 0, h = 0){
        return this.size(this.w + w, this.h + h);
    };
    fill(r = 0, g = r, b = r, a = 255){
        if (!this.color) this.color = [{r: null, g: null, b: null, a: null}];
        if (this.color[0].r !== r || this.color[0].g !== g || this.color[0].b !== b || this.color[0].a !== a){
            this.color = [{r, g, b, a}];
            if (this.clientside){
                if (this.htmlMode){
                    if (this.color.length === 1) {
                        //? Single Colors
                        const color = this.color[0];
                        const rgbaString = `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a / 255})`;
                        this.graphics.style.background = '';
                        this.graphics.style.backgroundColor = rgbaString;
                    };
                };
                this._redraw();
            };
        };        
        return this;
    };
    _stroke(){
        const [r, g, b, weight, intrinsic] = [this.strokeObj.r, this.strokeObj.g, this.strokeObj.b, this.strokeObj.weight, this.strokeObj.intrinsic];
        if (this.clientside){
            if (this.htmlMode){
                if (this.sides === 4){
                    if (intrinsic){
                        this.graphics.style.border = `${weight}px solid rgb(${r},${g},${b})`; 
                        this.graphics.style.boxSizing = 'border-box';
                    } else {
                        this.graphics.style.outline = `${weight}px solid rgb(${r},${g},${b})`; 
                    };
                } else {
                    //? Custom bordering using 2 new elements
                    // Note: This wont apply an inner stroke to shapes with a hole in it.
                    const offset = intrinsic ? 0 : weight;
                    if (!this.outerProp){
                        this.outerProp = new Box(true);
                        this.innerProp = new Box(true);
                    };
                    this.graphics.style.visibility = 'hidden';
                    this.outerProp 
                        .size(this.w+offset*2, this.h+offset*2)
                        .position(this.x - offset, this.y - offset)
                        .fill(r, g, b)
                        .shape(this.rememberedVal)
                        .rotate(this.radians, true)
                    this.innerProp
                        .size(this.w-weight*2+offset*2, this.h-weight*2+offset*2)
                        .position(this.x + weight - offset, this.y + weight - offset)
                        .shape(this.rememberedVal)
                        .rotate(this.radians, true)
                    if (this.color.length === 1){
                        this.innerProp.fill(this.color[0].r, this.color[0].g, this.color[0].b)
                    } else {
                        this.innerProp.gradient(this.color, this.radial)
                    };
                    if (this.src) this.innerProp.image(this.src, this.rememberedClipping);
                    if (this.textObj?.content) this.innerProp.text(this.textObj.content, this.textObj);
                };          
            } else {
                this.graphics.lineStyle({
                    width: weight, 
                    color: rgb(r, g, b),
                    alignment: intrinsic ? 0.5 : 1, 
                });
            };
        };
        return this;
    };
    stroke(r = 0, g = r, b = r, weight = 1, intrinsic = true, scaling = true){
        this.strokeObj = {r, g, b, weight, intrinsic, scaling};
        this._stroke()
        this._redraw();
        return this;
    };
    line(x1, y1, x2, y2, width = 1){
        if (x1 == null || x2 == null || y1 == null || y2 == null){
            throw new Error("You must pass 4 coordinate points into \"line\"")
        };
        const obj = createLinearRect(x1, y1, x2, y2, width);
        this.size(obj.width, obj.height);
        this.position(obj.x, obj.y);
        this.rotate(obj.radians, true);
        return this;
    };
    run(callback){
        this.callback = callback;
        return this;
    };
};


