

/*
How to use this mini-library:

For graphics, first consider using speedrunjs* provided in this mini-library. Otherwise you will need a graphics library for drawing ellipses and lines to simulate the node tours.

To use, call tspApproximator(points, iterations);

Points represent an array of coordinate points with x and y values (such as [{x:1,y:1}, {x:2, y:2}]), 
    Iterations will affect how long it runs. Default is 32.

*The version of speedrunjs used in this project is still in beta, and theres no plans to update it within this mini-library. See speedrunjs for future project updates. This version will however render the graphics you need to visualize the provided solutions.

*/
{
   
    function calculateEuclideanDistance(point1, point2) {
        const dx = point1.x - point2.x;
        const dy = point1.y - point2.y;
        return Math.sqrt(dx * dx + dy * dy);
    };

    function calculateDistanceOnAxis(point1, point2) {
        return Math.abs(point1 - point2);
    }
    
    function calculateTotalDistance(permutation) {
        if (!permutation || permutation.length < 2) {
            console.error("Invalid input to calculateTotalDistance", permutation);
        };
        let totalDistance = 0;
        for (let i = 0; i < permutation.length - 1; i++) {
            const currentDistance = calculateEuclideanDistance(permutation[i], permutation[i + 1]);
            totalDistance += currentDistance;
        }
        totalDistance += calculateEuclideanDistance(permutation[permutation.length - 1], permutation[0]);
        return totalDistance;
    };
    
    

    function moveEndToFront(arr) {
        if (arr.length < 2) {
            return arr;
        };
        const lastElement = arr.pop();
        arr.unshift(lastElement);
        return arr;
    };

    function calculateCenterPoint(points) {
        const n = points.length;
        let centerX = 0;
        let centerY = 0;
        for (const point of points) {
            centerX += point.x;
            centerY += point.y;
        };
        return {
            x: centerX / n,
            y: centerY / n
        };
    };

    const generateCacheKey = (points, ...args) => {
        const pointsKey = points.map(point => `${point.x}_${point.y}`).join('-');
        const argsKey = args.join('_');
        return `${pointsKey}|${argsKey}`;
    };

    
    function calculateAngle(point, centerPoint, factor = 1) {
        return Math.atan2(point.y - centerPoint.y, point.x - centerPoint.x)*factor;
    };
    function normalizeRadians(radians) {
        radians = (radians % (2 * Math.PI) + 2 * Math.PI) % (2 * Math.PI);
        return radians / (2 * Math.PI);
      };
    
    function calculateCenterPointBetweenTwo(point1, point2) {
        const centerX = (point1.x + point2.x) / 2;
        const centerY = (point1.y + point2.y) / 2;
        return { x: centerX, y: centerY };
    };    

    const findNearestNeighbors = (points, centerPoint) => {
        for (let i = 0; i < points.length; i++){
            points[i]._nearestNeighbors = [];
            let iterations = 6;
            for (let j = 0; j < iterations; j++){
                points[i]._nearestNeighbors.push(([...points]).sort((a,b)=>{
                    let distanceA = calculateEuclideanDistance(a, points[i]);
                    let distanceB = calculateEuclideanDistance(b, points[i]);
                    if (j === 0) return (distanceA - distanceB);
                    let centerPointDistanceA = calculateEuclideanDistance(a, centerPoint);
                    let centerPointDistanceB = calculateEuclideanDistance(b, centerPoint);
                    if (j === 1) return (distanceA/centerPointDistanceA - distanceB/centerPointDistanceB);
                    let angleI = normalizeRadians(calculateAngle(centerPoint, points[i]));
                    let angleA = normalizeRadians(calculateAngle(centerPoint, a));
                    let angleB = normalizeRadians(calculateAngle(centerPoint, b));
                    let angleDifferenceA = Math.abs(angleA - angleI);
                    let angleDifferenceB = Math.abs(angleI - angleB);
                    if (j === 2) return  ((distanceA + (distanceA * angleDifferenceA)) / centerPointDistanceA) - 
                                        ((distanceB + (distanceB * angleDifferenceB)) / centerPointDistanceB);
                    if (j === 3) return ((distanceA + (distanceA * angleDifferenceA)*2) / centerPointDistanceA) - 
                                    ((distanceB + (distanceB * angleDifferenceB)*2) / centerPointDistanceB);
                    if (j === 4) return ((distanceA + (distanceA * angleDifferenceA))) - ((distanceB + (distanceB * angleDifferenceB)));
                    if (j === 5) return ((distanceA + distanceA/centerPointDistanceA) - (distanceB + distanceB/centerPointDistanceB));
                }));
            };
        };
    };

    const efficientNearestNeighbor = (points, centerPoint) => {
        if (!points[0]._nearestNeighbors) findNearestNeighbors(points, centerPoint);
        const allTours = [];
        for (let v = 0; v < points[0]._nearestNeighbors.length; v++){
            let nextPoint = points[0];
            const usedNodes = new Set([nextPoint]);
            for (let i = 0; i < points.length; i++){
                const neighbors = nextPoint._nearestNeighbors[v];
                for (let j = 1; j < neighbors.length; j++){ // We always skip the first one, because the closest is always itself
                    if (!usedNodes.has(neighbors[j])){
                        usedNodes.add(neighbors[j]);
                        nextPoint = neighbors[j];
                        break;
                    };
                };
            };
            const tour = [...usedNodes];
            const dist = calculateTotalDistance(tour);
            allTours.push({tour: tour, dist: dist, variant: v});
        };
        return allTours;
    };
    

    function optimizeTour(tour, groupSize = 6, cache, startIDX = 0, endIDX = tour.length) {
        const originalTourLength = tour.length;
        if (groupSize > 6) groupSize = 6;
        groupSize+= 2;
        const cacheKey = generateCacheKey(tour,`optimizeTour|${groupSize}|${startIDX}|${endIDX}|`);
        if (cache && cache.hasOwnProperty(cacheKey)) { 
            return [...cache[cacheKey]]; // Retrieve From Cache
        };
        
        function calculateDistance(pointA, pointB) {
            return Math.sqrt(Math.pow(pointA.x - pointB.x, 2) + Math.pow(pointA.y - pointB.y, 2));
        };
        function calculateSegmentDistance(segment) {
            let distance = 0;
            for (let i = 0; i < segment.length - 1; i++) {
                distance += calculateDistance(segment[i], segment[i + 1]);
            }
            return distance;
        };    
        function segmentKey(segment) {
            return segment.map(p => `optimizeTour|segmentKey|${p.x},${p.y}`).join('|');
        };
        function permute(permutation) {
            let length = permutation.length,
                result = [permutation.slice()],
                c = new Array(length).fill(0),
                i = 1, k, p;
    
            while (i < length) {
                if (c[i] < i) {
                    k = i % 2 ? c[i] : 0;
                    p = permutation[i];
                    permutation[i] = permutation[k];
                    permutation[k] = p;
                    ++c[i];
                    i = 1;
                    result.push(permutation.slice());
                } else {
                    c[i] = 0;
                    ++i;
                };
            };
            return result;
        };
        let n = tour.length;
        for (let i = startIDX; i < Math.min(n - groupSize, endIDX); i++) {
            let end = (i + groupSize - 1) % n;
            let segment = [];
            for (let j = i; j != (end + 1) % n; j = (j + 1) % n) {
                segment.push(tour[j]);
            };
            let key = segmentKey(segment);
            let bestDistance = calculateSegmentDistance(segment);
            let bestPermutation = segment.slice();
            if (cache[key]) {
                bestPermutation = cache[key];
            } else {
                let permutations;
                permutations = permute(segment.slice(1, -1));
                permutations.forEach(permutation => {
                    let newSegment = [segment[0]].concat(permutation).concat(segment[segment.length - 1]);
                    let newDistance = calculateSegmentDistance(newSegment);
                    if (newDistance < bestDistance) {
                        bestDistance = newDistance;
                        bestPermutation = newSegment;
                    };
                });
                cache[key] = bestPermutation;
            };
            for (let j = 0, k = i; j < bestPermutation.length; j++, k = (k + 1) % n) {
                tour[k] = bestPermutation[j];
            };
        };
        
        tour = [...new Set(tour)];
        if (tour.length === originalTourLength) cache[cacheKey] = [...tour];
        return tour;
    };







    const shortestTour = (tour1, tour2, dist1 = null, dist2 = null) => {
        if (!dist1) dist1 = calculateTotalDistance(tour1);
        if (!dist2) dist2 = calculateTotalDistance(tour2);
        if (dist2 < dist1 && tour2.length === tour1.length){
            return [tour2, dist2, true];
        } else {
            return [tour1, dist1, false];
        };
    };
    
    const swapIntersectingEdges = (tour, minDistance, cache, startIDX = 0, endIDX = tour.length) => {
        const cacheKey = generateCacheKey(tour, `swapIntersectingEdges`, `|${startIDX}|`, `|${endIDX}|`);
        if (cache && cache.hasOwnProperty(cacheKey)) { 
            return [...cache[cacheKey]]; // Retrieve From Cache
        };
        let points = [...tour];
        const swapSubtour = (points, startIdx, endIdx) => {
            while (startIdx < endIdx) {
                [points[startIdx], points[endIdx]] = [points[endIdx], points[startIdx]];
                startIdx++;
                endIdx--;
                startIdx = startIdx % points.length;
                endIdx = (endIdx + points.length) % points.length;
            };
        };
        const identifyIntersectingEdges = (points, callback) =>{
            for (let i = 0; i < points.length; i++){
                points[i]._i = i;
            }
            const shareVertices = (line1, line2) => {
                if ((line1.x1 === line2.x1 && line1.y1 === line2.y1) ||
                    (line1.x1 === line2.x2 && line1.y1 === line2.y2) ||
                    (line1.x2 === line2.x1 && line1.y2 === line2.y1) ||
                    (line1.x2 === line2.x2 && line1.y2 === line2.y2)){
                    return true;
                } else {
                    return false;
                };
            };
            const lines = [];
            for (let i = startIDX; i < Math.min(endIDX, points.length); i++){
                const j = (i+1)%points.length;
                lines.push({x1: points[i].x, y1: points[i].y, 
                            x2: points[j].x, y2: points[j].y, 
                            p1: points[i],   p2: points[j]});
            };
            lines.sort((a,b)=> Math.min(a.x1, a.x2) - Math.min(b.x1, b.x2));
            for (let i = 0; i < lines.length; i++){
                for (let j = i + 1; j < lines.length; j++){
                    if (!shareVertices(lines[i], lines[j]) && doLineSegmentsIntersect(lines[i], lines[j])){
                        const fourPoints = [lines[i].p1, lines[i].p2, lines[j].p1, lines[j].p2];
                        callback(fourPoints);
                    };
                    if (Math.min(lines[j].x1, lines[j].x2) > Math.max(lines[i].x1, lines[i].x2)) break;
                };
            };
        };
        let improvedAtAll = false;
        let improved = false;
        let prevDistance = minDistance;
        for (let iterations = 0; iterations < Math.ceil(Math.sqrt(points.length)); iterations++){
            points = [...tour];
            identifyIntersectingEdges(points, ([p1, p2, p3, p4]) => {
                let combinations = [
                    [p1._i, p2._i], [p1._i, p3._i], [p1._i, p4._i],
                    [p2._i, p1._i], [p2._i, p3._i], [p2._i, p4._i],
                    [p3._i, p4._i], [p3._i, p2._i], [p3._i, p1._i],
                ];
                combinations.forEach(([startIdx, endIdx]) => {
                    swapSubtour(points, startIdx, endIdx);
                    let newDistance = calculateTotalDistance(points);
                    if (newDistance < minDistance) {
                        minDistance = newDistance;
                        tour = [...points];
                    };
                    swapSubtour(points, startIdx, endIdx);
                });
                combinations.forEach(([startIdx, endIdx]) => {
                    swapSubtour(points, startIdx+1, endIdx-1);
                    let newDistance = calculateTotalDistance(points);
                    if (newDistance < minDistance) {
                        minDistance = newDistance;
                        tour = [...points];
                    };
                    swapSubtour(points, startIdx+1, endIdx-1);
                });
            });
            if (minDistance < prevDistance){
                improved = true;
                improvedAtAll = true;
            };
            if (!improved) break;
            improved = false;
            prevDistance = minDistance;
        };
        cache[cacheKey] = [[...tour], minDistance, improvedAtAll];
        return [tour, minDistance, improvedAtAll];
    };

    const efficient2Opt = (p, minDist, cache, quadratic = true, singleSwap = true, tourSwap = true, startIDX = 0, endIDX = p.length) => {
        const cacheKey = generateCacheKey(p, `efficient2Opt`, `|${quadratic}|`, `|${singleSwap}|`, `|${tourSwap}|`, `|${startIDX}|`, `|${endIDX}|`);
        if (cache && cache.hasOwnProperty(cacheKey)) { 
            let [tour, dist] = [...cache[cacheKey]]; // Retrieve From Cache
            return [[...tour], dist];
        };
        const swapSubtour = (points, startIdx, endIdx) => {
            while (startIdx < endIdx) {
                [points[startIdx], points[endIdx]] = [points[endIdx], points[startIdx]];
                startIdx++;
                endIdx--;
                startIdx = startIdx % points.length;
                endIdx = (endIdx + points.length) % points.length;
            };
        };
        const calculateSingleSwap = (points, i, j) => {
            const iPrev = (i - 1 + points.length) % points.length;
            const iNext = (i + 1) % points.length;
            const jPrev = (j - 1 + points.length) % points.length;
            const jNext = (j + 1) % points.length;
            let preDistance, postDistance;
            if (iNext === j) {
                // When i and j are adjacent and j is after i
                preDistance = calculateEuclideanDistance(points[i], points[iPrev]) + calculateEuclideanDistance(points[j], points[jNext]);
                postDistance = calculateEuclideanDistance(points[j], points[iPrev]) + calculateEuclideanDistance(points[i], points[jNext]);
            } else if (jNext === i) {
                // When i and j are adjacent and i is after j
                preDistance = calculateEuclideanDistance(points[j], points[jPrev]) + calculateEuclideanDistance(points[i], points[iNext]);
                postDistance = calculateEuclideanDistance(points[i], points[jPrev]) + calculateEuclideanDistance(points[j], points[iNext]);
            } else {
                // General case
                preDistance = calculateEuclideanDistance(points[i], points[iPrev]) + calculateEuclideanDistance(points[i], points[iNext]) +
                              calculateEuclideanDistance(points[j], points[jPrev]) + calculateEuclideanDistance(points[j], points[jNext]);
                postDistance = calculateEuclideanDistance(points[j], points[iPrev]) + calculateEuclideanDistance(points[j], points[iNext]) +
                               calculateEuclideanDistance(points[i], points[jPrev]) + calculateEuclideanDistance(points[i], points[jNext]);
            };
            return minDistance - preDistance + postDistance;
        };
        const calculateSwapDistance = (points, i, j) => {
            const iNext = (i + 1) % points.length;
            const jNext = (j + 1) % points.length;
            const preDistance = calculateEuclideanDistance(points[i], points[iNext]) + calculateEuclideanDistance(points[j], points[jNext]);
            const postDistance = calculateEuclideanDistance(points[i], points[j]) + calculateEuclideanDistance(points[iNext], points[jNext]);
            return minDistance - preDistance + postDistance;
        };
        let points = [...p];
        let minDistance = minDist;
        for (let i = startIDX; i < Math.min(points.length, endIDX); i++) {
            let neighbors = points;
            let secondArrayStartIDX = 0;
            if (points[i]._nearestNeighbors){
                neighbors = points[i]._nearestNeighbors[0];
                secondArrayStartIDX = 1;
            };
            const iterations = quadratic ? neighbors.length : Math.ceil(Math.sqrt(neighbors.length));
            for (let j = secondArrayStartIDX; j < iterations; j++) {
                const endIdx = (i + j) % points.length;
                if (i !== endIdx) {
                    if (tourSwap){
                        let virtualDistSubtour = calculateSwapDistance(points, i, endIdx);
                        if (virtualDistSubtour < minDistance) {
                            swapSubtour(points, (i + 1) % points.length, endIdx);
                            minDistance = virtualDistSubtour;
                        };   
                    };                     
                    if (singleSwap){
                        let virtualDistSingleSwap = calculateSingleSwap(points, i, endIdx);
                        if (virtualDistSingleSwap < minDistance) {
                            [points[i], points[endIdx]] = [points[endIdx], points[i]];
                            minDistance = virtualDistSingleSwap;
                        };
                    };                                  
                };
            };
        };   
        const finalDistance = calculateTotalDistance(points);
        if (finalDistance < minDist) {
            cache[cacheKey] = [[...points], finalDistance];
            return [points, finalDistance];
        } else {
            cache[cacheKey] = [[...p], minDist];
            return [p, minDist];
        };
    };



    const edgeEjection = (p, minDistance, cache) => {
        let points = [...p];
        const cacheKey = generateCacheKey(points, `edgeEjection`);
        if (cache && cache.hasOwnProperty(cacheKey)) { 
            let [tour, dist, improved] = [...cache[cacheKey]]; // Retrieve From Cache
            return [[...tour], dist, improved];
        };
        const serializePoint = (point) => `x${point.x}y${point.y}`;
        const indices = new Map(points.map((point, index) => [serializePoint(point), index]));
        const distance = (a, b) => Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2);
        const len = points.length;
        const n = (m) => (m + len) % len; // Normalize Indices
        const distanceDifference = (fromIndex, toIndex, segmentLength) => {
            if (toIndex < 0 || toIndex >= points.length || fromIndex === toIndex) return Infinity;
            const reach = segmentLength - 1;
            const mStart = points[n(fromIndex)]; // Moving/Origin Start
            const mEnd = points[n(fromIndex+reach)]; // Moving/Origin End
            const mPrev = points[n(fromIndex-1)]; // Before Moving Start
            const mAfter = points[n(fromIndex+reach+1)]; // After Moving End
            const sStart = points[n(toIndex-1)]; // Stationary/Destination Start
            const sEnd = points[n(toIndex)]; // Stationary/Destination End
            const originDisconnection = distance(mStart, mPrev) + distance(mEnd, mAfter);
            const originReconnection = distance(mPrev, mAfter);
            const destinationConnection = distance(mStart, sStart) + distance(mEnd, sEnd);
            const destinationDisconnection = distance(sStart, sEnd);
            const netChange = (originReconnection + destinationConnection) - (originDisconnection + destinationDisconnection);
            return netChange;
        };
        const moveSegment = (array, fromIndex, segmentLength, toIndex) => {
            if (fromIndex === toIndex) return array; // No operation needed
            const segment = array.splice(fromIndex, segmentLength);
            if (fromIndex < toIndex) {
                toIndex -= segmentLength; // Adjust toIndex after segment removal
            }
            array.splice(toIndex, 0, ...segment);
            return array;
        };
        let improvedAtAll = false;
        let improved = false;
        for (let iterations = 0; iterations < 10; iterations++){
            improved = false;
            const maxDislocate = Math.ceil(Math.sqrt(points.length));
            for (let length = maxDislocate; length > 0; length--) {
                for (let i = 0; i < points.length; i++){
                    const point = points[i];
                    const nearestNeighbors = point._nearestNeighbors[0];
                    const depth =  Math.ceil(Math.sqrt(points.length)) + 1;
                    for (let j = 1; j < Math.min(depth, points.length - 1); j++){
                        const nnIndex = indices.get(serializePoint(nearestNeighbors[j]));
                        if (nnIndex == null || !points[nnIndex]) {
                            continue; // Error handling should no longer be needed, but will remain in case
                        };
                        const distDiff = distanceDifference(i, nnIndex, length);
                        if (distDiff < 0) { //! Bug: Theres more reports of negative distDiffs than there are actual improvements
                            let tempPoints = [...points];
                            moveSegment(tempPoints, i, length, nnIndex);
                            const newDistance = calculateTotalDistance(tempPoints);
                            if (newDistance < minDistance) {
                                improved = true;
                                improvedAtAll = true;
                                minDistance = newDistance; 
                                indices.clear();
                                points = tempPoints;
                                points.forEach((p, index) => indices.set(serializePoint(p), index));

                            };
                        }
                    }
                };
            };
            if (!improved) break;
        };
        cache[cacheKey] = [[...points], minDistance, improvedAtAll];
        return [points, minDistance, improvedAtAll];
    };


    const fineEdgeCorrection = (p, minDistance, cache = null, quadratic = true, startIDX = 0, endIDX = p.length) => {
        let improved = false;
        const cacheKey = generateCacheKey(p, `fineEdgeCorrection`, `|${quadratic}|`, `|${startIDX}|`, `|${endIDX}|`);
        if (cache && cache.hasOwnProperty(cacheKey)) { 
            let [tour, dist] = [...cache[cacheKey]]; // Retrieve From Cache
            return [[...tour], dist];
        };
        let points = [...p];
        const optimizeFactor = quadratic ? Math.min(points.length-1, 8) : Math.min(points.length-1, 6);
        let improved1, improved2, improved3, improved4;
            for (let i = 0; i < 10; i++){
                [points, minDistance, improved1] = efficient2Opt(points, minDistance, cache, quadratic, true, true, startIDX, endIDX);        
                [points, minDistance, improved2] = shortestTour(points, optimizeTour(points, optimizeFactor, cache, startIDX, endIDX), minDistance);
                [points, minDistance, improved3] = swapIntersectingEdges(points, minDistance, cache, startIDX, endIDX); 
                //[points, minDistance, improved4] = edgeEjection(points, minDistance);
            if (!improved1 && !improved2 && !improved3 /*&& !improved4*/){
                break;
            } else {
                improved = true;
            }
            points.reverse();
        };
        minDistance = calculateTotalDistance(points);
        if (cache) cache[cacheKey] = [[...points], minDistance]; // Add to Cache
        return [points, minDistance, improved];
    };

    function rotateCycleToFirstPoint(points, center = {x: 0, y: 0}) {
        // Calculate the distance of a point from the center
        function distanceFromCenter(point) {
            return Math.sqrt((point.x - center.x) ** 2 + (point.y - center.y) ** 2);
        }
    
        // Identify the point closest to the center
        let closestPointIndex = 0;
        let minDistance = distanceFromCenter(points[0]);
        points.forEach((point, index) => {
            const distance = distanceFromCenter(point);
            if (distance < minDistance) {
                closestPointIndex = index;
                minDistance = distance;
            }
        });
    
        // Rotate the cycle so that the closest point is first
        const rotatedCycle = [...points.slice(closestPointIndex), ...points.slice(0, closestPointIndex)];
        return rotatedCycle;
    };

    const rotateArray = (arr, N) => {
        N = N % arr.length; 
        return arr.slice(-N).concat(arr.slice(0, -N));
    };

    //? Split a good tour and rework the remaining solution using other variants of nearest neighbor
    const geneticallySplice = (p, minDistance, testing) => {
        let points = [...p];
        const extraTours = [];
        const halfValue = points.length/2;
        const minimumValue = Math.min(Math.max(Math.ceil(Math.sqrt(points.length)), 9), points.length); // Term2: Min 9, max len, but preferably sqrt(n)
        const averageValue = Math.min(Math.ceil((halfValue + minimumValue)/2), points.length);
        let vals = [halfValue, averageValue, minimumValue]; 
        const partialNearestNeighbor = (points, len, variant) => {
            let nextPoint = points[points.length - 1];
            const usedNodes = new Set(points);
            for (let i = points.length - 1; i < len; i++){
                const neighbors = nextPoint._nearestNeighbors[variant];
                for (let j = 1; j < neighbors.length; j++){ // We always skip the first one, because the closest is always itself
                    if (!usedNodes.has(neighbors[j])){
                        usedNodes.add(neighbors[j]);
                        nextPoint = neighbors[j];
                        break;
                    };
                };
            };
            const tour = [...usedNodes];
            const dist = calculateTotalDistance(tour);
            return [tour, dist];
        };
        let iterations = Math.min(points.length, Math.ceil(Math.sqrt(points.length)*2));
        let prevDist = minDistance;
        const runs = 5;
        for (let i = 0; i < runs; i++){
            for (let iteration = 0; iteration < iterations; iteration++){
                for (let val = 0; val < vals.length; val++){
                    const vLen = points[0]._nearestNeighbors.length;
                    for (let variant = 0; variant < vLen; variant++){
                        const childGenes = [...points].splice(points.length - vals[val]);
                        let [childPoints, childDistance] = partialNearestNeighbor(childGenes, points.length, variant);
                        if (childDistance + 2e-5 < minDistance){
                            extraTours.push({tour: childPoints, dist: childDistance});
                        };
                        [points, minDistance] = shortestTour(points, childPoints, minDistance, childDistance);
                    };
                };
                points = rotateArray(points, Math.floor(points.length/iterations));
            };
            if (minDistance >= prevDist) break;
            prevDist = minDistance;
        };        
        return [points, minDistance, extraTours];
    };

    const runNearestNeighbor = (points, minDistance, centerPoint, cubic) => {
        //? Sort points by farthest from center to closest
            points.sort((a,b)=>{
                const distFromCenterA = calculateEuclideanDistance(a, centerPoint);
                const distFromCenterB = calculateEuclideanDistance(b, centerPoint);
                return distFromCenterB - distFromCenterA;
            })
        //? Calculate Nearest Neighbor Tours
            const allTours = [];
            let tourIterations = Math.ceil(Math.sqrt(points.length));
            if (cubic) tourIterations = points.length;
            for (let i = 0; i < tourIterations; i++){
                const newTour = efficientNearestNeighbor(points, centerPoint);
                for (let j = 0; j < newTour.length; j++){
                    allTours.push(newTour[j]);
                };
                points = moveEndToFront(points);
            };
            allTours.sort((a,b)=> a.dist - b.dist);
        //? Our default best Nearest Neighbor tour
            [points, minDistance] = [allTours[0].tour, allTours[0].dist];
        return [points, minDistance, allTours];
    };

    function normalizePoints(points) {
        let minX = Math.floor(Math.min(...points.map(p => p.x)));
        let minY = Math.floor(Math.min(...points.map(p => p.y)));
        const fix = 10;
        let normalizedPoints = points.map(p => ({ x: (p.x - minX).toFixed(fix)*1, y: (p.y - minY).toFixed(fix)*1 }));
        return [ normalizedPoints, minX, minY ];
    };

    function denormalizePoints(normalizedPoints, minX, minY) {
        let denormalizedPoints = normalizedPoints.map(p => ({ x: p.x + minX, y: p.y + minY }));
        return denormalizedPoints;
    };
    
    

    function splitAndSplice(points, minDistance, cache) {
        const cacheKey = generateCacheKey(points, `splitAndSplice`);
        if (cache && cache.hasOwnProperty(cacheKey)) { 
            let [tour, dist, improved] = [...cache[cacheKey]]; // Retrieve From Cache
            return [[...tour], dist, improved];
        };
        let improved = false;    
        const splitSegments = (p, numberOfSegments) => {
            const points = [...p];
            const segments = [];
            const segmentSize = Math.floor(points.length / numberOfSegments);
            for (let i = 0; i < numberOfSegments; i++) {
                const start = i * segmentSize;
                const end = i === numberOfSegments - 1 ? points.length : (i + 1) * segmentSize;
                segments.push(points.slice(start, end));
            }
            return segments;
        };
    
        const recombineSegments = (segments) => {
            return segments.reduce((acc, segment) => [...acc, ...segment], []);
        };
        let quantity = Math.ceil(Math.sqrt(points.length));
        let factor = 2;
            let num = quantity;
            for (let i = 0; i < points.length; i+= num) {
                const segments = splitSegments(points, factor);
                segments.forEach((segment, index) => {
                    let segmentDistance = calculateTotalDistance(segment);
                        [segment, segmentDistance] = fineEdgeCorrection(segment, segmentDistance, cache, true, points.length - quantity, points.length);
                        [segment, segmentDistance] = fineEdgeCorrection(segment, segmentDistance, cache, true, 0, quantity);
                        [segment, segmentDistance] = fineEdgeCorrection(segment, segmentDistance, cache, false);
                    segments[index] = segment;
                });
                let tour = recombineSegments(segments);
                let tourDistance = calculateTotalDistance(tour);
                    [tour, tourDistance] = fineEdgeCorrection(tour, tourDistance, cache, true, points.length - quantity, points.length);
                    [tour, tourDistance] = fineEdgeCorrection(tour, tourDistance, cache, true, 0, quantity);
                    [tour, tourDistance] = fineEdgeCorrection(tour, tourDistance, cache, false);
                tour = [...new Set(tour)];
                if (tourDistance < minDistance && tour.length === points.length) {
                    improved = true;
                    points = tour;
                    minDistance = tourDistance;
                }
                points = rotateArray(points, num);
            }
        cache[cacheKey] = [[...points], minDistance, improved];
        return [points, minDistance, improved];
    };   

    const tspApproximator = (points, iterations = 32) => {
        //? Normalize points at 0,0 to ensure we avoid potential discrepancy bugs
            let normalizedMinX, normalizedMinY;
            [points, normalizedMinX, normalizedMinY] = normalizePoints(points);
        let cache = {};
        let allTours;
        let minDistance = calculateTotalDistance(points);
        //? Find centerPoint
            const centerPoint = calculateCenterPoint(points);
        //? Find best nearest neighbor
            [points, minDistance, allTours] = runNearestNeighbor(points, minDistance, centerPoint, false);
            
        //? Genetically Splice Superior Child Tour
            let splicedPoints, splicedDistance;
            [splicedPoints, splicedDistance] = geneticallySplice(points, minDistance);
            [points, minDistance] = shortestTour(points, splicedPoints, minDistance, splicedDistance);
            [points, minDistance] = fineEdgeCorrection(points, minDistance, cache, false);      

        let trialTours = [];
        trialTours[0] = {tour: points, dist: minDistance};
        let distances = new Set([minDistance]);
        for (let i = 0; i < iterations; i++){
            if (allTours[i] && !distances.has(allTours[i].dist)){
                trialTours.push(allTours[i]);
                distances.add(allTours[i].dist);
            };
            if (!allTours[i]) break;
        };

        //? Exhaustively refine best solution
        for (let t = 0; t < trialTours.length; t++){
            let tour = trialTours[t].tour;
            let tourDist = trialTours[t].dist;
            let maxIterations = 10;
            let [improved1, improved2] = [false, false];
            for (let i = 0; i < maxIterations; i++){
                [tour, tourDist, improved1] = splitAndSplice(tour, tourDist, cache);
                [tour, tourDist, improved2] = edgeEjection(tour, tourDist, cache);
                if (!improved1 && !improved2) break;
                tour = rotateArray(tour, 1);
            };
            if (tourDist < minDistance){
                points = tour;
                minDistance = tourDist;
            };
        };

        //? Denormalize points to get back our original coordinate points
            const sortedPoints = denormalizePoints(points, normalizedMinX, normalizedMinY);
            sortedPoints._nearestNeighbors = null;

        return [sortedPoints, minDistance];
    };

    const geneticAntColonySystem = (points) => {
        let cache = {};
        let normalizedMinX, normalizedMinY;
            [points, normalizedMinX, normalizedMinY] = normalizePoints(points);
        let bestTours = [];
        const centerPoint = calculateCenterPoint(points);
        findNearestNeighbors(points, centerPoint);
        //? Sort Nodes By Distance To Center
            const sortedNodes = ([...points]).sort((a,b)=>{ 
                const distFromCenterA = calculateEuclideanDistance(a, centerPoint);
                const distFromCenterB = calculateEuclideanDistance(b, centerPoint);
                return distFromCenterB - distFromCenterA;
            });
        //? Define PRNG
            let minDistance = calculateTotalDistance(sortedNodes);
            const prng = new PRNG(Math.floor(minDistance));
        //? Give Points IDs
        for (let i = 0; i < sortedNodes.length; i++){
            sortedNodes[i]._id = i;
        };
        function calculateNewAverage(previousAverage, numberOfItems, newNumber) {
            const newTotal = (previousAverage * numberOfItems) + newNumber;
            const newAverage = newTotal / (numberOfItems + 1);
            return newAverage;
        };
        //? Define Pheromone Map
        function convertPheromoneMapToArray(pheromoneMap) {
            const nodeConnections = {};
            // Iterate through the map to organize data into node connections
            pheromoneMap.forEach((value, key) => {
              const nodes = key.split('|').map(Number);
              nodes.forEach((node, index) => {
                if (!nodeConnections[node]) {
                  nodeConnections[node] = [];
                }
                const otherNode = nodes[(index + 1) % 2]; // Gets the other node in the pair
                nodeConnections[node].push({ index: otherNode, val: value.val });
              });
            });
          
            // Convert the intermediate object to an array of arrays, sorting connections by value
            const result = Object.keys(nodeConnections).map(nodeIndex => {
              const connections = nodeConnections[nodeIndex];
              // Ensure numerical sorting of node indices
              const sortedIndex = parseInt(nodeIndex, 10);
              // Sort connections by descending order of their value
              connections.sort((a, b) => b.val - a.val);
              return connections;
            });
          
            // Create an array to hold the sorted nodes, ensuring no gaps for nodes without connections
            const sortedNodes = [];
            result.forEach((connections, index) => {
              // Use the actual node index for correct placement
              sortedNodes[index] = connections;
            });
          
            return sortedNodes;
          };          
                  
        const initializePheromoneMaps = (allTours) => {
            if (!allTours || allTours.length <= 0){
                allTours = [{tour: points, dist: Infinity, variant: null}];
            }
            const pheromoneMap = new Map();
            const heuristicTours = [];
                for (let i = 0; i < allTours.length; i++){
                    heuristicTours.push(allTours[i]);
                };
            //? Convert heuristicTours into pheromone maps
                for (let i = 0; i < heuristicTours.length; i++){
                    const tour = heuristicTours[i].tour;
                    for (let j = 0; j < tour.length; j++){
                        const [curr, next] = [j, (j+1)%tour.length];
                        const [minID, maxID] = [Math.min(tour[curr]._id, tour[next]._id), Math.max(tour[curr]._id, tour[next]._id)];
                        const connectionID = `${minID}|${maxID}`;
                        if (!pheromoneMap.has(connectionID)){
                            pheromoneMap.set(connectionID, {val: 0, nodes: [tour[minID], tour[maxID]]});
                        };
                        const connection = pheromoneMap.get(connectionID);
                        const currentValue = connection.val;
                        const tourDistanceFraction = heuristicTours[i].dist / tour.length;
                        let pheromoneDeposit = 1 / (tourDistanceFraction**2);
                        const newValue = currentValue + pheromoneDeposit;
                        connection.val = newValue;
                    };
                };
            return pheromoneMap;
        };

        //? Define Our Ants
            const scoutAnts = [];
            const numScoutAnts = points.length + 1000;
            const numVariants = points[0]._nearestNeighbors.length - 1;
            for (let i = 0; i < numScoutAnts; i++){
                    scoutAnts.push(
                        {
                        //? Which point the ant starts on
                            startingNode: prng.integerBetween(0, points.length - 1), 
                        //? The nearest neighbor variant adhered to
                            variant: prng.integerBetween(0, numVariants), 
                        //? The tendency to deviate from the locally best available option
                            randomPreference: prng.decimalBetween(0.5/points.length, 2.5/points.length),
                            randomnessMultiplier: prng.decimalBetween(1, 2), // Is more random during splices
                        //? The tendency to follow pheromones over nearest neighbor heuristics
                            pheromonePreference: prng.random(),
                        //? The tendency to switch to a different node
                            wanderingProbability: prng.decimalBetween(0, 0.5),
                            weightedWanderingProbability: prng.decimalBetween(0, 0.3),
                            wanderingDistance: prng.integerBetween(2, 8),
                        //? Personal Stats
                            personalBest: Infinity,
                            averageRun: null,
                            numRuns: 0,
                            numSuccesses: 0,
                            numImprovements: 0,
                        });
            };
        const navigatePheromones = (scoutAnts, pheromoneArray, sortedNodes, points, minDistance, iteration, stagnation) => {
            const newTours = [];
            let improved = false;
            let counter = 0;
            let spliceTour = iteration >= 2 ? true : false;
            let spliceNumber, spliceIndex, reverseTour, splicedTour, splicedTourIds;
            spliceNumber = points.length;
            if (spliceTour){
                if (stagnation == undefined) stagnation = 0;
                if (stagnation > 1) stagnation = 0.75;
                let maxSpliceNumber = points.length - 3;
                let minSpliceNumber = Math.max(3, Math.floor((maxSpliceNumber - 3)*stagnation));
                spliceNumber = prng.integerBetween(minSpliceNumber, maxSpliceNumber);
                spliceIndex = prng.integerBetween(0, Math.ceil(points.length/2));
                reverseTour = prng.random() < 0.1;
                splicedTour = [...points];
                if (spliceIndex) splicedTour = rotateArray(splicedTour, spliceIndex);
                if (reverseTour) splicedTour.reverse();
                splicedTour.splice(spliceNumber);
                splicedTourIds = splicedTour.map(point => point._id);
            };
            for (let i = 0; i < scoutAnts.length; i++){
                let newTour = spliceTour ? [...splicedTour] : [];
                const usedNodes = spliceTour ? new Set(splicedTourIds) : new Set();
                const variant = scoutAnts[i].variant;
                let nextIndex = scoutAnts[i].startingNode;
                if (spliceTour && nextIndex < splicedTour.length - 1) nextIndex = splicedTour.length - 1;
                let nextPoint = sortedNodes[nextIndex];
                //? Wander to different node
                if (prng.random() < scoutAnts[i].wanderingProbability){
                    if (prng.random() < scoutAnts[i].weightedWanderingProbability){
                        const farNode = prng.integerBetween(0, Math.ceil(Math.sqrt(points.length)));
                        scoutAnts[i].startingNode = prng.integerBetween(0, farNode);
                    } else {
                        scoutAnts[i].startingNode = prng.integerBetween(0, points.length - 1);
                    };
                };
                for (let j = 0; j < sortedNodes.length; j++){
                    newTour.push(nextPoint);
                    usedNodes.add(nextPoint._id);
                    const candidates = pheromoneArray[nextPoint._id];
                    let changed = false;
                    const choosePheromonesOverHeuristics = prng.random() > scoutAnts[i].pheromonePreference;
                    counter++;
                    const randomValue = prng.random();
                    let chooseNextNode; 
                    let randomnessMultiplier = Math.max(1, (scoutAnts[i].randomnessMultiplier - 1) * (points.length/spliceNumber));
                    chooseNextNode = (randomValue < scoutAnts[i].randomPreference*randomnessMultiplier);
                    if (choosePheromonesOverHeuristics){
                        for (let k = 0; k < candidates.length; k++){
                            const index = candidates[k].index;
                            if (chooseNextNode){
                                continue;
                            };
                            if (!usedNodes.has(index)){
                                nextPoint = sortedNodes[index];
                                changed = true;
                                break;
                            };                   
                        };
                    } else {
                        const nearestNeighbors = nextPoint._nearestNeighbors[variant];
                        for (let k2 = 0; k2 < nearestNeighbors.length; k2++){
                            if (chooseNextNode){
                                continue;
                            };
                            if (!usedNodes.has(nearestNeighbors[k2]._id)){
                                nextPoint = nearestNeighbors[k2];
                                changed = true
                                break;
                            };
                        }; 
                    }
                    //? If we are unable to find a pair, perform nearest neighbor
                    if (!changed){
                        const nearestNeighbors = nextPoint._nearestNeighbors[variant];
                        for (let k2 = 0; k2 < nearestNeighbors.length; k2++){
                            if (!usedNodes.has(nearestNeighbors[k2]._id)){
                                nextPoint = nearestNeighbors[k2];
                                break;
                            };
                        }; 
                    };
                };
                newTour = [...new Set(newTour)];
                let newTourDistance = calculateTotalDistance(newTour);
                newTours.push({tour: newTour, dist: newTourDistance, variant: variant});
                scoutAnts[i].averageRun = calculateNewAverage(scoutAnts[i].averageRun || newTourDistance, scoutAnts[i].numRuns || 1, newTourDistance);
                if (newTourDistance < scoutAnts[i].personalBest){
                    scoutAnts[i].personalBest = newTourDistance;
                    scoutAnts[i].numImprovements++;
                };
                scoutAnts[i].numRuns++;
                if (newTourDistance < minDistance){
                    const [finetunedPoints, finetunedDistance] = fineEdgeCorrection(points, minDistance, cache, true);
                    bestTours.push({tour: finetunedPoints, dist: finetunedDistance});
                    improved = true;
                    if (iteration > 0) scoutAnts[i].numSuccesses++; // No ant is credited for round 1
                    points = newTour;
                    minDistance = newTourDistance;
                };
            };
            return [points, minDistance, newTours, improved];
        };
        let newTours;
        //? Generate Initial Solutions and Determine Effectiveness of Each Ant
            const initialIterations = 10;
            for (let i = 1; i < initialIterations; i++){
                const pheromoneMap = initializePheromoneMaps(newTours);
                const pheromoneArray = convertPheromoneMapToArray(pheromoneMap);
                [points, minDistance, newTours] = navigatePheromones(scoutAnts, pheromoneArray, sortedNodes, points, minDistance, i);
            };
        //? Sort Ants by performance: Historical winners come first, then personal bests, then average runs
            scoutAnts.sort((a,b)=> b.numSuccesses - a.numSuccesses || a.personalBest - b.personalBest || a.averageRun - b.averageRun);
        //? Get rid of the unproductive ants
            const successorCount = 100 + Math.ceil(Math.sqrt(points.length));
            scoutAnts.splice(successorCount);
        //? Continue The Experiment With The Best Ants
        //if (testing){
            let improved;
            let consecutiveNonImprovements = 0;
            let stagnationLimit = 16;
            for (let i = initialIterations; i < successorCount; i++){
                const pheromoneMap = initializePheromoneMaps(newTours);
                const pheromoneArray = convertPheromoneMapToArray(pheromoneMap);
                const stagnation = consecutiveNonImprovements/stagnationLimit;
                [points, minDistance, newTours, improved] = navigatePheromones(scoutAnts, pheromoneArray, sortedNodes, points, minDistance, i, stagnation);
                if (improved){
                    consecutiveNonImprovements = 0;
                } else {
                    consecutiveNonImprovements++;
                };
            };
            scoutAnts.sort((a,b)=> b.numSuccesses - a.numSuccesses || a.personalBest - b.personalBest || a.averageRun - b.averageRun);
        bestTours.sort((a,b)=>a.dist-b.dist);
        [points, minDistance] = [bestTours[0].tour, bestTours[0].dist];
        points = denormalizePoints(points, normalizedMinX, normalizedMinY);
        return [points, minDistance];
    };

    //! ANT COLONY SYSTEM TEST CODE
    function antColonyOptimization(points, alpha = 1, beta = 5, evaporation = 0.5, antCount = 100, iterations = 100) {
        const numberOfCities = points.length;
        const distance = calculateDistanceMatrix(points);
        let pheromone = initializePheromoneMatrix(numberOfCities, points);
        let bestPath = points;
        let bestLength = calculateTotalDistance(bestPath);
        class Ant {
            constructor(alpha, beta, numberOfCities) {
                this.alpha = alpha; // Pheromone importance
                this.beta = beta; // Distance priority
                this.numberOfCities = numberOfCities;
                this.tabu = []; // Cities to be visited
                this.currentCity = this.randomCity();
                this.path = [this.currentCity];
                this.pathLength = 0;
            };
            randomCity() {
                return Math.floor(Math.random() * this.numberOfCities);
            };
            moveToNextCity(pheromone, distance) {
                this.tabu.push(this.currentCity);
                const probabilities = this.calculateProbabilities(pheromone, distance);
                this.currentCity = this.selectNextCity(probabilities);
                this.path.push(this.currentCity);
            };
            calculateProbabilities(pheromone, distance) {
                const probabilities = [];
                let pheromoneSum = 0;
                for (let i = 0; i < this.numberOfCities; i++) {
                    if (!this.tabu.includes(i)) {
                        const tau = Math.pow(pheromone[this.currentCity][i], this.alpha);
                        const eta = Math.pow(1.0 / distance[this.currentCity][i], this.beta);
                        pheromoneSum += tau * eta;
                    };
                };
                for (let i = 0; i < this.numberOfCities; i++) {
                    if (this.tabu.includes(i)) {
                        probabilities[i] = 0;
                    } else {
                        const tau = Math.pow(pheromone[this.currentCity][i], this.alpha);
                        const eta = Math.pow(1.0 / distance[this.currentCity][i], this.beta);
                        probabilities[i] = (tau * eta) / pheromoneSum;
                    };
                };
                return probabilities;
            };
            selectNextCity(probabilities) {
                let cumProb = 0;
                const rand = Math.random();
                for (let i = 0; i < probabilities.length; i++) {
                    cumProb += probabilities[i];
                    if (rand <= cumProb) {
                        return i;
                    };
                };
                return this.tabu[0];
            };
            calculatePathLength(distance) {
                let length = 0;
                for (let i = 0; i < this.path.length - 1; i++) {
                    length += distance[this.path[i]][this.path[i + 1]];
                };
                length += distance[this.path[this.numberOfCities - 1]][this.path[0]]; // Return to start
                this.pathLength = length;
            };
        };
        function calculateDistanceMatrix(points) {
            const distanceMatrix = [];
            for (let i = 0; i < points.length; i++) {
                distanceMatrix[i] = [];
                for (let j = 0; j < points.length; j++) {
                    if (i === j) {
                        distanceMatrix[i][j] = 0;
                    } else {
                        const dx = points[i].x - points[j].x;
                        const dy = points[i].y - points[j].y;
                        distanceMatrix[i][j] = Math.sqrt(dx * dx + dy * dy);
                    };
                };
            };
            return distanceMatrix;
        };
        function initializePheromoneMatrix(numberOfCities, precomputedPath, initialValue = 1.0) {
            const pheromoneMatrix = [];
            for (let i = 0; i < numberOfCities; i++) {
                pheromoneMatrix[i] = new Array(numberOfCities).fill(initialValue);
            }
            if (precomputedPath && precomputedPath.length > 0) {
                // Increase pheromone levels on the precomputed path
                for (let i = 0; i < precomputedPath.length - 1; i++) {
                    let from = precomputedPath[i];
                    let to = precomputedPath[i + 1];
        
                    if (from >= 0 && from < numberOfCities && to >= 0 && to < numberOfCities) {
                        pheromoneMatrix[from][to] += (initialValue*10); // or another value that signifies stronger pheromone
                        pheromoneMatrix[to][from] = pheromoneMatrix[from][to];
                    };
                }
            };        
            return pheromoneMatrix;
        };
        for (let iter = 0; iter < iterations; iter++) {
            let ants = [];
            for (let i = 0; i < antCount; i++) {
                ants.push(new Ant(alpha, beta, numberOfCities));
            };
            for (let i = 0; i < numberOfCities - 1; i++) {
                for (let ant of ants) {
                    ant.moveToNextCity(pheromone, distance);
                };
            };
            for (let ant of ants) {
                ant.calculatePathLength(distance);
                if (ant.pathLength < bestLength) {
                    bestLength = ant.pathLength;
                    bestPath = ant.path.map(index => points[index]); // Convert indices to point objects
                };
            };
            for (let i = 0; i < numberOfCities; i++) {
                for (let j = 0; j < numberOfCities; j++) {
                    pheromone[i][j] *= (1 - evaporation);
                };
            };
            for (let ant of ants) {
                for (let i = 0; i < ant.path.length - 1; i++) {
                    let from = ant.path[i];
                    let to = ant.path[i + 1];
                    pheromone[from][to] += (1.0 / ant.pathLength);
                    pheromone[to][from] = pheromone[from][to]; 
                };
            };
        };
        return bestPath;
    };




//? Helper function to parse data 
function parseTSP(data, scale = 0) {
    // Normalize line endings and split the data into lines
    const lines = data.replace(/\r\n/g, '\n').split('\n');
    // More robustly find the start and end of the coordinates section
    const coordsStart = lines.findIndex(line => line.trim() === 'NODE_COORD_SECTION') + 1;
    const coordsEnd = lines.findIndex(line => line.trim() === 'EOF', coordsStart);
  
    let coordinates = [];
  
    for (let i = coordsStart; i < coordsEnd; i++) {
      if (!lines[i].trim()) continue; // Skip empty lines
      const parts = lines[i].trim().split(/\s+/);
      // Ensure there are at least 3 parts (index, x, and y coordinates)
      if (parts.length < 3) continue;
      const x = parseFloat(parts[1]);
      const y = parseFloat(parts[2]);
      if (!isNaN(x) && !isNaN(y)) { // Ensure parsed values are numbers
        coordinates.push({ x, y });
      }
    }
  
    function scaleCoordinates(coordinates, maxSize) {
      let maxX = 0;
      let maxY = 0;
      coordinates.forEach(coord => {
        maxX = Math.max(maxX, coord.x);
        maxY = Math.max(maxY, coord.y);
      });
      const scaleFactor = maxSize ? Math.min(maxSize / maxX, maxSize / maxY) : 1;
      return coordinates.map(coord => ({
        x: coord.x * scaleFactor,
        y: coord.y * scaleFactor
      }));
    }
  
    if (scale && coordinates.length > 0) {
      return scaleCoordinates(coordinates, scale);
    }
    return coordinates;
  }

  const LKHTour = parseTSP(`
  NAME : Tnm199.tsp
COMMENT : 199-city problem, points on edges of tetrahedron. n = 55  m = 12 (Hougardy + Zhong)
TYPE : TSP
DIMENSION : 199
EDGE_WEIGHT_TYPE : EUC_2D
NODE_COORD_SECTION
1 545000 8660
2 270000 467654
3 10000 0
4 540000 17321
5 265000 458993
6 20000 0
7 535000 25981
8 260000 450333
9 30000 0
10 530000 34641
11 255000 441673
12 40000 0
13 525000 43301
14 250000 433013
15 50000 0
16 520000 51962
17 245000 424352
18 60000 0
19 515000 60622
20 240000 415692
21 70000 0
22 510000 69282
23 235000 407032
24 80000 0
25 505000 77942
26 230000 398372
27 90000 0
28 500000 86603
29 225000 389711
30 100000 0
31 495000 95263
32 220000 381051
33 110000 0
34 490000 103923
35 215000 372391
36 120000 0
37 485000 112583
38 210000 363731
39 130000 0
40 480000 121244
41 205000 355070
42 140000 0
43 475000 129904
44 200000 346410
45 150000 0
46 470000 138564
47 195000 337750
48 160000 0
49 465000 147224
50 190000 329090
51 170000 0
52 460000 155885
53 185000 320429
54 180000 0
55 455000 164545
56 180000 311769
57 190000 0
58 450000 173205
59 175000 303109
60 200000 0
61 445000 181865
62 170000 294449
63 210000 0
64 440000 190526
65 165000 285788
66 220000 0
67 435000 199186
68 160000 277128
69 230000 0
70 430000 207846
71 155000 268468
72 240000 0
73 425000 216506
74 150000 259808
75 250000 0
76 420000 225167
77 145000 251147
78 260000 0
79 415000 233827
80 140000 242487
81 270000 0
82 410000 242487
83 135000 233827
84 280000 0
85 405000 251147
86 130000 225167
87 290000 0
88 400000 259808
89 125000 216506
90 300000 0
91 395000 268468
92 120000 207846
93 310000 0
94 390000 277128
95 115000 199186
96 320000 0
97 385000 285788
98 110000 190526
99 330000 0
100 380000 294449
101 105000 181865
102 340000 0
103 375000 303109
104 100000 173205
105 350000 0
106 370000 311769
107 95000 164545
108 360000 0
109 365000 320429
110 90000 155885
111 370000 0
112 360000 329090
113 85000 147224
114 380000 0
115 355000 337750
116 80000 138564
117 390000 0
118 350000 346410
119 75000 129904
120 400000 0
121 345000 355070
122 70000 121244
123 410000 0
124 340000 363731
125 65000 112583
126 420000 0
127 335000 372391
128 60000 103923
129 430000 0
130 330000 381051
131 55000 95263
132 440000 0
133 325000 389711
134 50000 86603
135 450000 0
136 320000 398372
137 45000 77942
138 460000 0
139 315000 407032
140 40000 69282
141 470000 0
142 310000 415692
143 35000 60622
144 480000 0
145 305000 424352
146 30000 51962
147 490000 0
148 300000 433013
149 25000 43301
150 500000 0
151 295000 441673
152 20000 34641
153 510000 0
154 290000 450333
155 15000 25981
156 520000 0
157 285000 458993
158 10000 17321
159 530000 0
160 280000 467654
161 5000 8660
162 540000 0
163 275000 476314
164 0 0
165 550000 0
166 22917 13231
167 527083 13231
168 275000 449852
169 45833 26462
170 504167 26462
171 275000 423390
172 68750 39693
173 481250 39693
174 275000 396928
175 91667 52924
176 458333 52924
177 275000 370466
178 114583 66155
179 435417 66155
180 275000 344005
181 137500 79386
182 412500 79386
183 275000 317543
184 160417 92617
185 389583 92617
186 275000 291081
187 183333 105848
188 366667 105848
189 275000 264619
190 206250 119078
191 343750 119078
192 275000 238157
193 229167 132309
194 320833 132309
195 275000 211695
196 252083 145540
197 297917 145540
198 275000 185233
199 275000 158771
EOF`);
  
    //! Test Code Requires SpeedrunJS to run
    
    const nodes = [];
    const nodeCopies = [];
    const sortedNodes = [];
    const sortedNodeCopies = [];
    for (let i = 0; i < LKHTour.length; i++){
        const randomX = LKHTour[i].x;//random(100, 500);
        const randomY = LKHTour[i].y;//random(100, 500);
    /*for (let i = 0; i < 16; i++){
        const randomX = random(100, 500);
        const randomY = random(100, 500);*/
        const xOffset = 800;
    
        const node = new Box();
        node.size(5,5)
        node.fill(0)
        node.shape("ellipse")
        node.position(randomX, randomY)
        nodes.push({x: node.x+node.w/2, y: node.y+node.h/2})
    
        const nodeCopy = new Box();
        nodeCopy.size(5,5)
        nodeCopy.fill(0)
        nodeCopy.shape("ellipse")
        nodeCopy.position(randomX + xOffset, randomY)
        nodeCopies.push({x: nodeCopy.x+nodeCopy.w/2, y: nodeCopy.y+nodeCopy.h/2})
    };    
    
    //? Ant Colony
    {
        console.time("\nTSP Approximator")
        //const sortedPoints = antColonyOptimization(nodes); const minDistance = calculateTotalDistance(sortedPoints);
        //const [sortedPoints, minDistance] = geneticAntColonySystem(nodes);
        const [sortedPoints, minDistance ] = tspApproximator(nodes, 1);
        console.timeEnd("\nTSP Approximator")
        console.log("\nTSP Approximator: ")
        console.log("Minimum distance:", minDistance);
        //let [sortedPoints, minDistance] = [[], null];
        
        for (let i = 0; i < sortedPoints.length; i++){
            sortedNodes.push(sortedPoints[i]);
        };
        const len = sortedNodes.length;
        for (let i = 0; i < len; i++){
            const line = new Box()
            line.fill(255,0,100)
            line.line(sortedNodes[i].x, sortedNodes[i].y, sortedNodes[(i+1)%len].x, sortedNodes[(i+1)%len].y, 2)
        };
    };
    
    //? TSP Approximator
    {
        console.time("\nTSP Approximator")
        const [sortedPoints, minDistance ] = tspApproximator(nodeCopies, 10);
        console.timeEnd("\nTSP Approximator")
        console.log("\nTSP Approximator: ")
        console.log("Minimum distance:", minDistance);
        for (let i = 0; i < sortedPoints.length; i++){
            sortedNodeCopies.push(sortedPoints[i]);
        };
        const len = sortedNodeCopies.length;
        for (let i = 0; i < len; i++){
            const line = new Box()
            line.fill(255,0,0)
            line.line(sortedNodeCopies[i].x, sortedNodeCopies[i].y, sortedNodeCopies[(i+1)%len].x, sortedNodeCopies[(i+1)%len].y, 2)
        };
    };
    
}
